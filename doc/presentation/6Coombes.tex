\documentclass[t,aspectratio=169]{beamer}
\usepackage{multicol}
\usepackage{sansmathaccent}
\pdfmapfile{+sansmathaccent.map}
\definecolor{krc}{rgb}{0.521, 0.118, 0.369} % 1C    3D   {0.3, 0.3, 0.9}
\definecolor{osuorange}{rgb}{0.8235,0.3725,0.0824}
\definecolor{osupurple}{rgb}{0.65,0.05,0.55}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set up the meta-information for the PDF file
\hypersetup{
  pdftitle={Thirty Transcription Factors},
  pdfsubject={transcription factors},
  pdfauthor={Kevin R.~Coombes,
    Department of Biomedical Informatics,
    The Ohio State University,
    <coombes.3@osu.edu>
  },
  pdfkeywords={Thresher,hierarchical clustering,principal components analysis,outliers},
  pdfpagemode={None},
  pdfpagetransition=Replace,
  colorlinks,
  linkcolor=,%internal, deliberately left blank
  urlcolor=krc%external
  }

\usetheme[secheader]{Columbus}
% color the Sinput and Soutput chunks
\setbeamerfont{block body}{size=\small}
\usepackage[nogin]{Sweave}
\DefineVerbatimEnvironment{Sinput}{Verbatim} {xleftmargin=1em}
\DefineVerbatimEnvironment{Soutput}{Verbatim}{xleftmargin=1em}
\DefineVerbatimEnvironment{Scode}{Verbatim}{xleftmargin=1em}
\fvset{listparameters={\setlength{\topsep}{0pt}}}
\newenvironment<>{Schunk}{%
  \begin{actionenv}#1%
    \parskip=0pt\vspace{\topsep}
    \def\insertblocktitle{}%
    \par\vspace{\topsep}%
    \usebeamertemplate{block begin}}
  {\par\parskip=0pt\vspace{\topsep}%
    \usebeamertemplate{block end}%
  \end{actionenv}}

\mode
<presentation>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

%\setkeys{Gin}{width=\textwidth}

%%%%%%%%%%%%%%%%%%%%
% need the usual title, author, etc for splash page as well as title page
\title[Thirty Transcription Factors]{Thirty Biologically
  Interpretable Clusters of\\ Transcription Factors Determine Cancer Type} 
\author[\copyright\ Copyright 2017-18, Kevin R.~Coombes]{Kevin R. Coombes\\
  \textcolor{pms7532}{\tt coombes.3@osu.edu}
}
\institute{Department of Biomedical Informatics\\
  The Ohio State University
}
\titlegraphic{
  \includegraphics[width=3in]{./Figures/OSU-WexMedCtr-4C-Horiz-CMYK}
}
\date{20 June 2018}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% actually start creating frames
\begin{frame}[plain,noframenumbering]
  \titlepage
\end{frame}

\parskip=5pt plus3pt minus2pt  % probably belongs in a 'theme' style somewhere.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
\subsection{Hypothesis and Data}
\begin{frame}
\frametitle{Introduction}

\begin{multicols}{2}

\textcolor{osuscarlet}{Hypothesis:} Transcription factors contain
enough information to distinguish cancer types.

\hrule

\begin{itemize}
\item TCGA RNA-sequencing data, ``RSEM\_genes\_normalized'' 
\item 33 cancer types
\item 10,446 samples
  \begin{itemize}
  \item 9,326 primary; 395 metastases; 725 normal
  \end{itemize}
\item Number of samples per cancer type ranges between
  \begin{itemize}
  \item 45 (CHOL) -- 1212 (BRCA).
  \end{itemize}
\end{itemize}

\begin{itemize}
\item Go to the \textbf{TFCat Transcription Factor Database}${}^*$
  (\url{http://www.tfcat.ca/}).
\item Database uses \textbf{mouse} Entrez Gene IDs as their
  primary identifier; map to human.
\item Only retain genes that were manually curated with
  ``\textbf{strong}'' evidence of being transcription factors.
\item Final list: 486 transcription factors.
\end{itemize}

{\small

\textcolor{osuorange}{${}^*$Fulton DL, et al. Genome Biology 2009; 10:R29.}

}

\end{multicols}
\end{frame}

\newcommand{\krc}{\vskip0pt plus 1filll}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Principal Components}
\subsection{Polychrome}
\begin{frame}
\frametitle{Principal Components Analysis}

\begin{multicols}{2}

\begin{center}
  \includegraphics[width=72mm]{../../results/knitfigs/pca1_2-1}
\end{center}

\begin{center}
  \includegraphics[width=60mm]{../../results/knitfigs/scheme-1}
\end{center}

{\small 

Reference: \textcolor{osuorange}{Coombes KR, et al. Polychrome:
  Creating and Assessing Qualitative Palettes With Many
  Colors. \textit{J Statist Software}. To appear.}}

\end{multicols}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{PCDimenion}
\begin{frame}
\frametitle{Number of Principal Components}
\begin{multicols}{2}

\begin{itemize}
\item Bartlett's Method: 485 (useless)
\item Broken Stick Method: 15 (conservative?)
\item Automated Auer-Gervini: 29 (realistic?)
\end{itemize}


\begin{center}
  \includegraphics[width=72mm]{../../results/knitfigs/ag-1}
\end{center}

Reference: \textcolor{osuorange}{Auer P, Gervini D. Choosing
principal components: a new graphical method based on Bayesian model
selection. Commun Stat Simulat, 2008; 37: 962--977.}

Reference: \textcolor{osuorange}{Wang M, Kornblau SM, Coombes
  KR. Decomposing the Apoptosis Pathway Into Biologically
  Interpretable Principal Components. \textit{Cancer
    Informatics}. 2018 May 9;17:1176935118771082.}

CRAN implementation: PCDimension package.

\end{multicols}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Transcription Factor Principal Components}
\begin{frame}
\frametitle{More Principal Component Plots}
\begin{multicols}{3}
\begin{center}
  \includegraphics[width=44mm]{../../results/knitfigs/moreplots-1}

  \includegraphics[width=44mm]{../../results/knitfigs/moreplots-4}

  \includegraphics[width=44mm]{../../results/knitfigs/moreplots-5}

  \includegraphics[width=44mm]{../../results/knitfigs/moreplots-8}

  \includegraphics[width=44mm]{../../results/knitfigs/moreplots-11}

  \includegraphics[width=44mm]{../../results/knitfigs/moreplots-13}
\end{center}
\end{multicols}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Non-linearity}
\subsection{Stochastic Neighbor Embedding}
\begin{frame}
\frametitle{Non-linearity: Stochastic Neighbor Embedding}
\begin{multicols}{2}
\begin{center}
  \includegraphics[width=70mm]{../../results/knit10figs/cancers-1}

  \includegraphics[width=70mm]{../../results/knit10figs/nml-zoom-1}
\end{center}
\end{multicols}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Thresher}
\subsection{Clustering on Hyperspheres}
\begin{frame}
\frametitle{Thresher: Principal Components and Clustering on Hyperspheres}

\begin{multicols}{2}
\textcolor{osuscarlet}{Question:} Since we only need 30 principal
components, do we really need all 486 transcription factors?

\textcolor{osuscarlet}{Idea:} View the genes you want to cluster as
``features'' that produce ``weight vectors'' based on their
contributions to significant components. Outliers have short
length. Then cluster everything that remains based on their directions
in PC space.

\textcolor{blue}{Real Motivation: Sacrifices orthogonality for
a chance at getting biologically interpretable clusters.}

Reference: \textcolor{osuorange}{Banerjee A, et al. Clustering on the
  unit hypersphere using von Mises-Fisher distributions. J Mach Learn
  Res, 2005; 6: 1345--1382.}

Reference: \textcolor{osuorange}{Hornik K, Gr\"un B. movMF: An R
  package for fitting mixtures of von Mises-Fisher distributions. J
  Stat Software, 2014; 58(10): 1--31.}

Reference: \textcolor{osuorange}{Wang M, et al. Thresher: Determining
  the Number of Clusters while Removing Outliers. \textit{BMC
    Bioinformatics}. 2018; 19(1):9.}

\end{multicols}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Biological Components}
\begin{frame}
\frametitle{Thresher Results}
\begin{multicols}{2}
\begin{itemize}
\item Found 30 clusters, which we are currently calling
  \textcolor{osuscarlet}{biological components}.
\item Recorded the ``most commonly seen tissue'' in UniGene for the
  transcription factors in each cluster.
\item Averaged the expression of transcription factors in each
  cluster to get ``biological scores'' for each sample.
\end{itemize}

\begin{center}
  \includegraphics[width=78mm]{../../results/ManyFigs/sorted-BC4}
\end{center}

Gene List:
\texttt{
ASCL1
GSX1
MYT1
MYT1L
NEUROD1
NEUROD6
OLIG2
POU3F4
SOX1
SOX3
TBR1
}

UniGene Tissues: \texttt{brain}

\end{multicols}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Biological Components}
\begin{multicols}{2}
\begin{center}
Melanocytes
  \includegraphics[width=72mm]{../../results/ManyFigs/sorted-BC17}

Intestine:
  \includegraphics[width=72mm]{../../results/ManyFigs/sorted-BC26}

Cell Cycle (Embryonically Lethal):
  \includegraphics[width=72mm]{../../results/ManyFigs/sorted-BC1}

Angiogenesis (Embryonically Lethal):
  \includegraphics[width=72mm]{../../results/ManyFigs/sorted-BC29}
\end{center}

\end{multicols}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Thirty, Count 'Em, Thirty}
\begin{frame}[fragile]
\frametitle{Name That Cancer In 30 Transcription Factors}
\begin{multicols}{2}

For each of the thirty transcription factor clusters (a.k.a.
biological components), we selected the member that was most strongly
correlated with the average expression.

{\small

\begin{verbatim}
TAF2    CREB3L3 SOX17   OLIG2   CDKN2A
FOXK1   FOXM1   ZEB1    PHOX2B  TCF12   
WT1     TAF1C   VDR     HOXB7   NKX2-1  
MYOG    PAX3    SPDEF   PRRX2   NANOG   
POU6F1  JUNB    PIN1    REST    IKZF1   
CDX2    RPL7A   SOX2    WWTR1   ELF3   
\end{verbatim}
}

\vfill

\begin{center}
  \includegraphics[width=70mm]{../../results/knit8figs/thirty-1}
\end{center}

\end{multicols}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions}
\subsection{Acknowledgements}
\begin{frame}
\frametitle{Conclusions and Acknowledgements}

\begin{multicols}{2}
\begin{itemize}
\item \texttt{Polychrome} produces useful palettes with lots of colors.
\item \texttt{PCDimension} (by automating the Auer-Gervini algorithm)
  seems like the best method to determine the number of principal
  components.
\item \texttt{Thresher} produces biologically interpretable
  non-principal components (BCs).
\item A combination of UniGene and ToppGene provides sensible
  interpretations of the BCs derived from the transcription factors.
\end{itemize}

 \begin{itemize} 
  \item Dr. Zachary Abrams, OSU
  \item Mark Zucker, OSU
  \item Dr. Min Wang, OSU
  \item Dr. Amir Asiaee Taheri, OSU
  \item Dr. Lynne V. Abruzzo, OSU
  \item Dr. Steven Kornblau, UT MDACC
  \item Kristi Bushman, University of Pittsburgh.
  \item[]
  \item Grants: NSF No. 1440386; NIH/NCI P30 CA016058; R01 CA182905,
    NIH/NLM T15LM011270.
 \end{itemize}

\end{multicols}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TOC at end ...
\begin{frame}[plain,noframenumbering]
\begin{multicols}{2}
  \tableofcontents
\end{multicols}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
