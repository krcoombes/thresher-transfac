\documentclass[t]{beamer}
\usepackage{multicol}
\usepackage{sansmathaccent}
\pdfmapfile{+sansmathaccent.map}
\definecolor{krc}{rgb}{0.521, 0.118, 0.369} % 1C    3D   {0.3, 0.3, 0.9}
\definecolor{osuorange}{rgb}{0.8235,0.3725,0.0824}
\definecolor{osupurple}{rgb}{0.65,0.05,0.55}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set up the meta-information for the PDF file
\hypersetup{
  pdftitle={Pathway Building Blocks},
  pdfsubject={clustering},
  pdfauthor={Kevin R.~Coombes,
    Department of Biomedical Informatics,
    The Ohio State University,
    <coombes.3@osu.edu>
  },
  pdfkeywords={Thresher,hierarchical clustering,principal components analysis,outliers},
  pdfpagemode={None},
  pdfpagetransition=Replace,
  colorlinks,
  linkcolor=,%internal, deliberately left blank
  urlcolor=krc%external
  }

\usetheme[secheader]{Columbus}
% color the Sinput and Soutput chunks
\setbeamerfont{block body}{size=\small}
\usepackage[nogin]{Sweave}
\DefineVerbatimEnvironment{Sinput}{Verbatim} {xleftmargin=1em}
\DefineVerbatimEnvironment{Soutput}{Verbatim}{xleftmargin=1em}
\DefineVerbatimEnvironment{Scode}{Verbatim}{xleftmargin=1em}
\fvset{listparameters={\setlength{\topsep}{0pt}}}
\newenvironment<>{Schunk}{%
  \begin{actionenv}#1%
    \parskip=0pt\vspace{\topsep}
    \def\insertblocktitle{}%
    \par\vspace{\topsep}%
    \usebeamertemplate{block begin}}
  {\par\parskip=0pt\vspace{\topsep}%
    \usebeamertemplate{block end}%
  \end{actionenv}}

\mode
<presentation>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

%\setkeys{Gin}{width=\textwidth}

%%%%%%%%%%%%%%%%%%%%
% need the usual title, author, etc for splash page as well as title page
\title{Pathway Building Blocks} 
\author[\copyright\ Copyright 2016, Kevin R.~Coombes]{Kevin R. Coombes\\
  \textcolor{pms7532}{\tt coombes.3@osu.edu}\\
  Min Wang, Zachary Abrams, Kristi Bushman
}
\institute{Department of Biomedical Informatics\\
  The Ohio State University
}
\titlegraphic{
  \includegraphics[width=3in]{./Figures/OSU-WexMedCtr-4C-Horiz-CMYK}
}
\date{18 November 2016}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% actually start creating frames
\begin{frame}[plain,noframenumbering]
  \titlepage
\end{frame}

\parskip=5pt plus3pt minus2pt  % probably belongs in a 'theme' style somewhere.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
\begin{frame}
\frametitle{Regulation of Apoptosis (from Cell Signaling)}
\begin{center}
\includegraphics[height=72mm]{./Figures/Static/apoptosis}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Gene Clustering}
\section{The Plan}
\begin{frame}
\frametitle{Gene Clustering: The Plan}
\begin{enumerate}
\item Use a statistically sound method to estimate the number $D$ of
  principal components.
\item Identify outliers as features with weak/small loadings in the
  space of significant principal components. Remove the outliers.
\item The number $K$ of clusters of features should satisfy $D \le K
  \le 2D$. Use model-based clustering, with the Bayesian Information
  Criterion (BIC), to select the optimal number in this range.
\end{enumerate}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{N Components}
\begin{frame}
\frametitle{Step 1: Number of Significant Components}
Reference: \textcolor{osuorange}{Auer P, Gervini D. Choosing
principal components: a new graphical method based on Bayesian model
selection. Commun Stat Simulat, 2008; 37: 962--977.}

View the problem as one of model selection, where the model $M_D$ says
that there are $D$ components. Mathematically, says that eigenvalues
in the PC matrix satisfy
$$ \lambda_1 \ge \lambda_2 \ge \cdots \ge \lambda_D,
 \qquad \lambda_D > \lambda_{D+1},
 \qquad \lambda_{D+1} = \lambda_{D+2} = \cdots = \lambda_M $$

After specifying a prior distribution on $D$, they compute the
posterior probability of each model, and choose the model with the
maximum posterior probability.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Auer-Gervini Priors}
Use a family of prior distributions on the number $D$ of components.
$$ P(D) \propto exp(-\theta n D/ 2) $$
As $\theta \mapsto 0$, prior becomes flat: choose maximum possible
number of components. As $\theta \mapsto \infty$, prior increasingly
forces choice of zero components.

\begin{center}
  \includegraphics[width=106mm]{./Figures/ag-priors}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Evaluating All the Priors}
\begin{multicols}{2}
  \includegraphics[width=53mm]{./Figures/ag32}

Auer-Gervini plot the number $D$ of components as a function of the
prior parameter $\theta$.  This is a step function.  Longer steps mean
 more people would choose that value of $D$.
 \textcolor{osuscarlet}{Pick the ``highest step that is a reasonable length''.}

Our additions: [1] \textcolor{osuscarlet}{Define largest sensible
  $\theta$ as one that assigns $99\%$ probability to $D=0$.}  [2]
\textcolor{osuscarlet}{Test different definitions of ``reasonable''.}

\end{multicols}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{An Objective ``Reasonable'' Step Length}
\begin{itemize}
\item Idea is to separate the step lengths into two groups: ``small''
  and ``significantly large''.
\item Can be viewed as a clustering or a change point problem.
\item \textcolor{osuscarlet}{We developed an R package,
  \texttt{PCDimension}, that implemented $8$ criteria} for separation:
    \begin{enumerate}   \itemsep2pt
    \item[\textcolor{black}{1--4:}] TwiceMean, Kmeans, Kmeans3 and Spectral.
    \item[\textcolor{black}{5--8:}] Ttest, Ttest2, CPT and CpmFun. 
    \end{enumerate}   
\item \textcolor{osuscarlet}{Compared with ``best'' methods from earlier studies:}
  \begin{itemize}
  \item Broken-Stick method.
  \item Bartlett's method.
  \item Randomization-based procedure.
  \end{itemize}
\item Performed simulations with various data sizes and correlation
  structures. 
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Correlation Structures}
\begin{center}
\includegraphics[width=0.14\textwidth]{./Figures/Mat/mat4.jpg}\hskip1mm
\includegraphics[width=0.14\textwidth]{./Figures/Mat/mat5.jpg}\hskip1mm
\includegraphics[width=0.14\textwidth]{./Figures/Mat/mat6.jpg}\hskip1mm
\includegraphics[width=0.14\textwidth]{./Figures/Mat/mat7.jpg}\\
\vskip1mm
\includegraphics[width=0.14\textwidth]{./Figures/Mat/mat8.jpg}\hskip1mm
\includegraphics[width=0.14\textwidth]{./Figures/Mat/mat9.jpg}\hskip1mm
\includegraphics[width=0.14\textwidth]{./Figures/Mat/mat10.jpg}\hskip1mm
\includegraphics[width=0.14\textwidth]{./Figures/Mat/mat11.jpg}\\
\vskip1mm
\includegraphics[width=0.14\textwidth]{./Figures/Mat/mat12.jpg}\hskip1mm
\includegraphics[width=0.14\textwidth]{./Figures/Mat/mat13.jpg}\hskip1mm
\includegraphics[width=0.14\textwidth]{./Figures/Mat/mat14.jpg}\hskip1mm
\includegraphics[width=0.14\textwidth]{./Figures/Mat/mat15.jpg}\\
\vskip1mm
\includegraphics[width=0.14\textwidth]{./Figures/Mat/mat16.jpg}\hskip1mm
\includegraphics[width=0.14\textwidth]{./Figures/Mat/mat17.jpg}\hskip1mm
\includegraphics[width=0.14\textwidth]{./Figures/Mat/mat18.jpg}\hskip1mm
\includegraphics[width=0.14\textwidth]{./Figures/Mat/mat19.jpg}\\
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Simulation Results}
\begin{center}
\includegraphics[height=60mm]{./Figures/fig2(lwd=3_error_0).jpeg} 
\end{center}
\textcolor{osuscarlet}{Auer-Gervini} (\textcolor{pink}{CPT} or
\textcolor{magenta}{TwiceMean}) and 
\textcolor{green}{rnd-Lambda} are the best on average.  Auer-Gervini
is two orders of magnitude faster.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Identifying Outliers}
\begin{frame}
\frametitle{Step 2: Identifying Outliers}
\begin{multicols}{2}
  \includegraphics[height=67mm]{./Figures/thresher-023}

Computed the distance from the origin (in optimal PC space) for each
feature in 2500 simulations, each containing 2 outliers and about 20
features.  Plotted the distribution of distances for both signal
features (since we know ``truth'') and for noise (outliers).

\textcolor{osuscarlet}{Can separate signal from noise with greater
  than $99\%$ accuracy with a cutoff around $0.3$.}
\end{multicols}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Summary Statistics for 4 Correlation Structures}
\begin{center}
\includegraphics[width=0.22\textwidth]{./Figures/Mat/mat10.jpg}\hskip1mm
\includegraphics[width=0.22\textwidth]{./Figures/Mat/mat11.jpg}\hskip1mm
\includegraphics[width=0.22\textwidth]{./Figures/Mat/mat12.jpg}\hskip1mm
\includegraphics[width=0.22\textwidth]{./Figures/Mat/mat13.jpg}\\
\end{center} 
\begin{table}[!htbp]
\caption{Summary statistics for detecting good and bad features}
\centering
\renewcommand{\arraystretch}{1.3}
\global\long\def\~{\hphantom{0}}
{ %
\resizebox{0.9\columnwidth}{!}{
\begin{tabular}{@{\extracolsep{10pt}}lcccccccc@{}}
\hline
Scenarios    & \multicolumn{4}{c}{96 samples, 24 features}  & \multicolumn{4}{c}{24 samples, 96 features} \tabularnewline
\cline{2-5} \cline{6-9}
  Datasets  & Dataset 7  & Dataset 8 & Dataset 9  & Dataset 10 & Dataset 7  & Dataset 8 & Dataset 9  & Dataset 10 \tabularnewline
\hline
Sensitivity    & $0.949$ & $0.94$  & $0.991$  & $0.957$ & $0.741$ & $0.714$ & $0.841$ & $0.809$ \tabularnewline
Specificity    & $0.816$ & $0.859$ & $1$      & $0.998$ & $0.805$ & $0.813$ & $1$     & $0.923$ \tabularnewline
FDR            & $0.205$ & $0.145$ & $0$      & $0.001$ & $0.263$ & $0.242$ & $0$     & $0.044$ \tabularnewline
AUC            & $0.882$ & $0.9$   & $0.995$  & $0.978$ & $0.773$ & $0.763$ & $0.92$  & $0.866$ \tabularnewline
\hline
\end{tabular}} 
}
\end{table}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Clustering on Hyperspheres}
\begin{frame}
\frametitle{Step 3: Model-Based Clustering}
\textcolor{osuscarlet}{Goal:} Now that we know the number of
significant principal components, want to determine the number of
feature-clusters.

This only depends on the directions/angles, not on their magnitudes.
So, corresponds to \textcolor{osuscarlet}{points on a hypersphere in
  $D$-dimensional space.} 

Can apply model-based clustering if we have a spherical equivalent of
the Gaussian normal distribution.  Will fit a model for each value of
$K$ and use Bayesian Information Criterion (BIC) to select the best $K$.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Clustering on Hyperspheres}
\begin{center}
\includegraphics[width=80mm]{./Figures/vmf.png}
\end{center}

{\small\centering

Figure 1, Hornik and Gr\"un.

}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Clustering on Hyperspheres}
I'll spare you the details.  Key points are [1] Someone has already
figured out which distribution to use:

Reference: \textcolor{osuorange}{Banerjee A, Dhillon IS, Ghosh J, Sra
  S. Clustering on the unit hypersphere using von Mises-Fisher
  distributions. J Mach Learn Res, 2005; 6: 1345--1382.}

and [2] someone else has already implemented this mixture model in an
R package:

Reference: \textcolor{osuorange}{Hornik K, Gr\"un B. movMF: An R
  package for fitting mixtures of von Mises-Fisher distributions. J
  Stat Software, 2014; 58(10): 1--31.}

\rule{\textwidth}{1.5pt}

We built another R package, \texttt{Thresher}, that combined
Auer-Gervini to determine the PC dimension, outlier removal, and
feature clustering using mixtures of von Mises-Fisher distributions.

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Accuracy in Estimating the Number of Clusters I}
\begin{table}[!htbp]
\centering
\renewcommand{\arraystretch}{1.3}
\global\long\def\~{\hphantom{0}}
\caption{Mean absolute deviation between estimates and truth (\(96\times\textbf{24}\)). }{ %
  \vspace{-0.15in}
\resizebox{\columnwidth}{!}{
\begin{tabular}{@{\extracolsep{10pt}}lcccccccccccc@{}}
\hline
& \multicolumn{10}{c}{NbClust Top 10 Best Indices}  & \multicolumn{2}{c}{Thresher} \tabularnewline
\cline{2-11} \cline{12-13}
Methods    & trcovw & tracew & ratkowsky & mcclain & ptbiserial & tau & sdindex & kl & ccc & hartigan & CPT  & TwiceMean\tabularnewline
\hline
1 & 1.009	& 1.023	& 1.085	& 1.117	& 1.96	& 2.059 & 2.289	& 2.121	& 2.074	& 2.888	& \textcolor{osuscarlet}{\textbf{0.521}} & 0.801\tabularnewline
2 & 1.012	& 1.04	& 1.098	& 1.142	& 1.941	& 2.062 & 2.281	& 2.257	& 2.342	& 3.035	& 0.1 	& \textcolor{osuscarlet}{\textbf{0.099}}\tabularnewline
3 & 1.005	& 1.033	& 1.08	& 1.118	& 1.922	& 2.102 & 2.274	& 2.283	& 2.688	& 2.922	& 0.154	& \textcolor{osuscarlet}{\textbf{0.152}}\tabularnewline
4 & 0.778	& 0.965	& 0.912	& 0.89	& 0.598	& 0.509	 & 0.49	& 1.005	& 1.312	& 0.898	& \textcolor{osuscarlet}{\textbf{0.4}} & 0.425\tabularnewline
5 & 0.961	& 0.97	& 0.912	& 0.913	& 0.625	& 0.54	 & 0.555	& 1.024	& 1.841	& 0.935	& 0.171	& \textcolor{osuscarlet}{\textbf{0.141}}\tabularnewline
6 & 0.83	& 0.965	& 0.913	& 0.895	& 0.614	& 0.525	 & 0.496	& 1.079	& 1.616	& 0.998	& \textcolor{osuscarlet}{\textbf{0.469}}	& 0.474\tabularnewline
7 & 0.079	& \textcolor{osuscarlet}{\textbf{0.038}}	& 0.101	& 0.152	& 0.915	& 1.069	& 1.257	& 1.145	& 1.233	& 1.881	& 0.686	& 0.754\tabularnewline
8 & 0.091	& \textcolor{osuscarlet}{\textbf{0.035}} & 0.087	& 0.106	& 0.867	& 1.04	& 1.25	& 1.088	& 1.241	& 1.839	& 0.793	& 0.79\tabularnewline
9 & 1.059	& 1.033	& 1.079	& 1.118	& 1.973	& 2.11	 & 2.261	& 2.148	& 2.108	& 2.865	& 0.46	& \textcolor{osuscarlet}{\textbf{0.458}}\tabularnewline
10& 1.014	& 1.032	& 1.088	& 1.121	& 1.929	& 2.104	 & 2.251	& 2.176	& 2.18	& 2.877	& \textcolor{osuscarlet}{\textbf{0.535}}	& 0.593\tabularnewline
11& 0.565	& 0.031	& 0.084	& 0.144	& 0.967	& 1.087	 & 1.206	& 1.071	& \textcolor{osuscarlet}{\textbf{0.023}}	& 1.887	& 0.105	& 0.102\tabularnewline
12& 0.118	& \textcolor{osuscarlet}{\textbf{0.042}} & 0.113	& 0.123	& 0.926	& 1.08	& 1.215	& 1.104	& 0.47	& 1.851	& 0.129	& 0.13\tabularnewline
13& 1.665	& 1.965	& 1.903	& 1.869	& 1.165	& 0.987	 & 0.853	& 1.408	& 1.984	& 0.778	& \textcolor{osuscarlet}{\textbf{0.586}}	& 0.591\tabularnewline
14& 1.934	& 1.973	& 1.927	& 1.895	& 1.157	& 0.974	 & 0.844	& 1.444	& 2.237	& 0.803	& \textcolor{osuscarlet}{\textbf{0.111}}	& \textcolor{osuscarlet}{\textbf{0.111}}\tabularnewline
15& 0.819	& 0.967	& 0.918	& 0.899	& 0.596	& 0.517	 & \textcolor{osuscarlet}{\textbf{0.489}}	& 1.08	& 1.076	& 0.905	& 1.367	& 1.361\tabularnewline
16& 0.946	& 0.961	& 0.903	& 0.916	& 0.63	& 0.55	 & 0.505	& 0.989	& 1.555	& 0.885	& 0.299	& \textcolor{osuscarlet}{\textbf{0.296}}\tabularnewline
\hline
Average& 0.868 & 0.88  & 0.888 & 0.901 & 1.174 & 1.207 & 1.282 & 1.464 & 1.624 & 1.765 & \textcolor{osuscarlet}{\textbf{0.43}} & 0.455\tabularnewline
\hline
\end{tabular}} 
}
\end{table}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Accuracy in Estimating the Number of Clusters II}
\begin{table}[!htbp]
\centering
\renewcommand{\arraystretch}{1.3}
\global\long\def\~{\hphantom{0}}
\caption{Mean absolute deviation between estimates and truth (\(24\times\textbf{96}\)). }{ %
  \vspace{-0.15in}
\resizebox{\columnwidth}{!}{
\begin{tabular}{@{\extracolsep{10pt}}lcccccccccccc@{}}
\hline
  & \multicolumn{10}{c}{NbClust Top 10 Best Indices}  & \multicolumn{2}{c}{Thresher} \tabularnewline
\cline{2-11} \cline{12-13}
Methods & tracew & mcclain & ratkowsky & trcovw & scott & silhouette & sdindex	& kl	& tau	& ptbiserial & CPT  & TwiceMean\tabularnewline
\hline
1 & 1.016	& \textcolor{osuscarlet}{\textbf{1.009}}	& 1.066	& 1.084	& 1.177	& 1.515	& 2.252	& 1.834	& 2.369	& 2.295	& 1.141	& 1.146\tabularnewline
2 & 1.012	& 1.02	& 1.064	& 1.07	& 1.198	& 2.622	& 2.256	& 1.843	& 2.329	& 2.328	& \textcolor{osuscarlet}{\textbf{0.013}}	& \textcolor{osuscarlet}{\textbf{0.013}}\tabularnewline
3 & 1.008	& 1.015	& 1.06	& 1.073	& 1.199	& 1.669	& 2.26	& 1.842	& 2.38	& 2.333	& \textcolor{osuscarlet}{\textbf{0.024}}	& \textcolor{osuscarlet}{\textbf{0.024}}\tabularnewline
4 & 0.985	& 0.982	& 0.931	& 0.831	& 0.837	& 0.991	& \textcolor{osuscarlet}{\textbf{0.432}}	& 0.887	& 0.565	& 0.702	& 2.914	& 2.995\tabularnewline
5 & 0.986	& 0.99	& 0.932	& 0.901	& 0.831	& 0.942	& 0.452	& 0.843	& 0.558	& 0.68	& 0.804	& \textcolor{osuscarlet}{\textbf{0.336}}\tabularnewline
6 & 0.985	& 0.987	& 0.932	& 0.821	& 0.847	& 1.096	& \textcolor{osuscarlet}{\textbf{0.452}}	& 0.944	& 0.594	& 0.739	& 2.954	& 2.982\tabularnewline
7 & \textcolor{osuscarlet}{\textbf{0.009}} & 0.019	& 0.065	& 0.12	& 0.219	& 0.577	& 1.274	& 0.876	& 1.379	& 1.334	& 1.479	& 2.612\tabularnewline
8 & \textcolor{osuscarlet}{\textbf{0.008}} & 0.015	& 0.069	& 0.147	& 0.196	& 0.608	& 1.256	& 0.828	& 1.328	& 1.308	& 1.699	& 2.706\tabularnewline
9 & \textcolor{osuscarlet}{\textbf{1.009}} & 1.018	& 1.067	& 1.13	& 1.2	      & 1.58	& 2.25	& 1.842	& 2.366	& 2.338	& 1.149	& 1.149\tabularnewline
10& 1.013	& 1.008	& 1.073	& 1.08	& 1.232	& 1.591	& 2.228	& 1.899	& 2.32	& 2.287	& \textcolor{osuscarlet}{\textbf{1.007}}	& \textcolor{osuscarlet}{\textbf{1.007}}\tabularnewline
11& 0.017	& \textcolor{osuscarlet}{\textbf{0.014}}	& 0.072	& 0.807	& 0.214	& 0.616	& 1.2	      & 0.824	& 1.31	& 1.3	      & 0.034	& 0.037\tabularnewline
12& \textcolor{osuscarlet}{\textbf{0.012}} & 0.013	& 0.069	& 0.233	& 0.219	& 0.498	& 1.255	& 0.857	& 1.345	& 1.313	& 0.022	& 0.022\tabularnewline
13& 1.987	& 1.982	& 1.932	& 1.618	& 1.817	& 1.681	& 0.852	& 1.481	& \textcolor{osuscarlet}{\textbf{0.794}}	& 0.908	& 0.892	& 0.884\tabularnewline
14& 1.991	& 1.987	& 1.934	& 1.859	& 1.819	& 1.664	& 0.858	& 1.54	& \textcolor{osuscarlet}{\textbf{0.852}}	& 0.985	& 1.084	& 1.039\tabularnewline
15& 0.989	& 0.981	& 0.932	& 0.825	& 0.851	& 0.958	& \textcolor{osuscarlet}{\textbf{0.405}}	& 0.933	& 0.553	& 0.671	& 1.856	& 1.859\tabularnewline
16& 0.995	& 0.995	& 0.949	& 0.899	& 0.853	& 0.937	& \textcolor{osuscarlet}{\textbf{0.447}}	& 0.875	& 0.596	& 0.724	& 1.661	& 1.744\tabularnewline
\hline
Average& \textcolor{osuscarlet}{\textbf{0.876}}	& 0.877	& 0.884	& 0.906	& 0.919	& 1.222	& 1.258	& 1.26	& 1.352	& 1.39	& 1.171	& 1.285\tabularnewline
\hline
\end{tabular}} 
}
\end{table}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{AML RPPA Data}
\subsection{RPPA Technology}
\begin{frame}
\frametitle{AML RPPA Data}

\begin{center}
  \includegraphics[width=96mm]{./Figures/Static/CASP3clvd}
\end{center}
\begin{itemize}\parskip=0pt\itemsep=2pt\small
\item AML = acute myeloid leukemia (511 samples)
\item RPPA = Reverse Phase Protein (lysate) Array
  \begin{itemize}
  \item Micro dot blots; samples printed (Aushon 2470 arrayer) in a
    dilution series on nitrocellulose membranes fixed to glass
    microscope slides.
  \item Antibody-based detection ($> 200$ antibodies)
  \item Slides are scanned, spots quantified (using MicroVigene), then
    dilution series quantified (using SuperCurve) and normalized.
  \end{itemize}
\item Collaboration with Steve Kornblau at M.D. Anderson
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Apoptosis Pathway}
\begin{frame}[fragile]
\frametitle{Apoptosis Proteins in AML RPPA Data}
\begin{columns}[c]
\begin{column}{0.59\textwidth}
  \includegraphics[height=64mm]{./Figures/Static/apoptosis}
\end{column}\hfill\begin{column}{0.39\textwidth}
\begin{block}{}\small
\begin{verbatim}
AIFM1        ARC          
BAD          BAD.pS112  
BAD.pS136    BAD.pS155    
BAK1         BAX        
BCL2         BCL2L1       
BCL2L11      BID        
BIRC2        BIRC5        
BMI1         CASP3      
CASP3.cl175  CASP7.cl198  
CASP8        CASP9      
CASP9.cl315  CASP9.cl330  
MCL1         MDM2       
MDM4         PARP1        
PARP1.cl214  YAP1       
YAP1p        DIABLO       
TP53         TP53.pS15  
XIAP       
\end{verbatim}
\end{block}
blah
\end{column}
\end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{RPPA Data on Apoptosis pathway in AML Samples}
\begin{multicols}{2}
  \includegraphics[width=60mm]{./Figures/Cleaned/apoptosis-cleaned-01-scree}

No clear ``elbow'' in the plot. 

Possibly small gaps at two, four, or six components.

Broken-stick model supports four components.

\end{multicols}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{AML RPPA: Auer-Gervini}
\begin{center}
  \includegraphics[width=75mm]{./Figures/Cleaned/apoptosis-cleaned-02-bayes}
\end{center}

Auer-Gervini method supports six groups.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{AML RPPA: Loadings}
\begin{center}
  \includegraphics[width=38mm]{./Figures/Cleaned/apoptosis-cleaned-04A-loadings}\hskip1mm
  \includegraphics[width=38mm]{./Figures/Cleaned/apoptosis-cleaned-04B-loadings}\hskip1mm
  \includegraphics[width=38mm]{./Figures/Cleaned/apoptosis-cleaned-04C-loadings}
\end{center}

Colors indicate clusters identified by movMF within \texttt{Thresher}.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setkeys{Gin}{width=96mm}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Decomposing the Apoptosis Pathway}
\begin{frame}[fragile]
\frametitle{Heatmap, Based on One ``Signal''}
\begin{center}
  \includegraphics{./Figures/Cleaned/apoptosis-G5-cleaned-06-heat-cont}
\end{center}

Proteins:
\begin{block}{}
\begin{verbatim}
CASP3.cl175 CASP7.cl198 CASP9.cl315 MDM2       
\end{verbatim}
\end{block}

{\small

\textcolor{osuscarlet}{
Pochampally R1, Fodera B, Chen L, Shao W, Levine EA, Chen J.
\textcolor{osupurple}{
A 60 kd MDM2 isoform is produced by caspase cleavage in non-apoptotic
tumor cells.
}
Oncogene. 1998 Nov 19;17(20):2629-36.
}

}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Heatmap, Based on One ``Signal''}
\begin{center}
  \includegraphics{./Figures/Cleaned/apoptosis-G4-cleaned-06-heat-cont}
\end{center}

Proteins:
\begin{block}{}
\begin{verbatim}
BID   CASP3 CASP8 XIAP 
\end{verbatim}
\end{block}

{\small

\textcolor{osuscarlet}{
Ferreira KS1, Kreutz C, Macnelly S, Neubert K, Haber A, Bogyo M,
Timmer J, Borner C.
\textcolor{osupurple}{
Caspase-3 feeds back on caspase-8, Bid and XIAP in type I Fas
signaling in primary mouse hepatocytes.
}
Apoptosis. 2012 May;17(5):503-15.
}

}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Heatmap, Based on One ``Signal''}
\begin{center}
  \includegraphics[width=96mm]{./Figures/Cleaned/apoptosis-G1-cleaned-06-heat-cont}
\end{center}

Proteins:
\begin{block}{}
\begin{verbatim}
AIFM1  BCL2   DIABLO
\end{verbatim}
\end{block}

{\small

\textcolor{osuscarlet}{Arnoult D1, Gaume B, Karbowski M, Sharpe JC, Cecconi F, Youle RJ.
\textcolor{osupurple}{
Mitochondrial release of AIF and EndoG requires caspase activation
downstream of Bax/Bak-mediated permeabilization. 
}
EMBO J. 2003 Sep 1;22(17):4385-99}

}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Heatmap, Based on One ``Signal''}
\begin{center}
  \includegraphics{./Figures/Cleaned/apoptosis-G2-cleaned-06-heat-cont}
\end{center}

Proteins:
\begin{block}{}
\begin{verbatim}
ARC       BAD.pS112 YAP1p    
\end{verbatim}
\end{block}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Heatmap, Based on One ``Signal''}
\begin{center}
  \includegraphics{./Figures/Cleaned/apoptosis-G3-cleaned-06-heat-cont}
\end{center}

Proteins:
\begin{block}{}
\begin{verbatim}
BAD.pS136 BAD.pS155 BAX      
\end{verbatim}
\end{block}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Heatmap, Based on One ``Signal''}
\begin{center}
  \includegraphics{./Figures/Cleaned/apoptosis-G6-cleaned-06-heat-cont}
\end{center}

Proteins:
\begin{block}{}
\begin{verbatim}
 BAD         BAK1        BCL2L1      BCL2L11    
 BIRC2       BIRC5       BMI1        CASP9      
 CASP9.cl330 MCL1        MDM4        PARP1      
 PARP1.cl214 YAP1        TP53        TP53.pS15  
\end{verbatim}
\end{block}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setkeys{Gin}{width=106mm}
\section{IL4 Signaling Pathway}
\subsection{IL4 Signaling}
\begin{frame}
  \frametitle{IL4 Signaling Pathway}
  \begin{columns}[T]
%  [  Wiki Pathway: Interleukin-4 Signaling Pathway. ]
\begin{column}{0.51\textwidth}
  \includegraphics[width=62mm]{./Figures/IL4}
\end{column}\hfill\begin{column}{0.47\textwidth}
  \begin{itemize}
  \item\small Interleukin 4 (IL4) induces a signaling cascade.
  \item\small Downstream effects include differentiation of Th2 cells.
  \item\small Primary signaling is through JAK/Stat6 and MAPKs.
  \item\small IL4 and its receptors have been shown to be overexpressed in many cancers.
  \item\small Patterns are homogeneous in normal tissue.
  \item[]
  \item\small \textcolor{osuorange}{Diagram from WikiPathways.}
  \end{itemize}
\end{column}
\end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{IL4 Signaling in Lung}
\begin{frame}
\frametitle{IL4 Signaling in Lung Cancer}
\begin{itemize}
\item \footnotesize{Three sets of RNA-Seq data from TCGA: Normal lung;
  Lung squamous cell carcinoma (LUSC); Lung adenocarcinoma (LUAD).}
\item[] \footnotesize{\qquad Normal \qquad\qquad\qquad\qquad\qquad LUSC \qquad\qquad\qquad\qquad\quad LUAD}
\end{itemize}
\begin{figure}
  \begin{tabular}{ccc}
    \includegraphics[width=0.3\textwidth, height=0.45\textwidth]{./Figures/NORMAL_IL4_dense.png} \quad
    \includegraphics[width=0.3\textwidth, height=0.45\textwidth]{./Figures/LUSC_IL4_dense.png}\quad
    \includegraphics[width=0.3\textwidth, height=0.45\textwidth]{./Figures/LUAD_IL4_dense.png}
  \end{tabular}
\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{IL4 Signaling across Cancer}
\begin{frame}
\frametitle{IL4 Signaling Across Cancer Types}

\begin{columns}
\begin{column}[c]{37mm}
  \begin{center}
  \includegraphics[width=35mm]{./Figures/Merge_IL4_2_CPM_18types_dense}
  \end{center}
\end{column}
\begin{column}[c]{75mm}
  \begin{center}
  \includegraphics[width=73mm]{{./Figures/heatmap.mean.score1.redgreen.exp1.25}.jpeg}
  \end{center}
\end{column}
\begin{column}[c]{15mm}
  \begin{center}
  \includegraphics[width=13mm]{./Figures/heatlegend}
  \end{center}
\end{column}
\end{columns}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Liver Hepatocellular Carcinoma (LIHC)}

\definecolor{dorange}{rgb}{1,0.75,0}
\definecolor{mycyan}{rgb}{0,1,1}
\setbeamercolor{tag1}{fg=black,bg=mycyan}
\setbeamercolor{tag5}{fg=black,bg=dorange}

\begin{center}

{\usebeamercolor{tag1}\colorbox{bg}{\color{fg}Cluster 1:}}
 \raisebox{-.5\height}{\includegraphics[width=90mm]{"./Figures/Scores1/score1 (no scale) cluster1".png}}

{\usebeamercolor{tag5}\colorbox{bg}{\color{fg}Cluster 5:}}
 \raisebox{-.5\height}{\includegraphics[width=90mm]{"./Figures/Scores1/score1 (no scale) cluster5".png}}
\end{center}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Low Grade Glioma (LGG)}

\setbeamercolor{tag2}{fg=black,bg=red}
\setbeamercolor{tag3}{fg=black,bg=yellow}
\begin{center}

{\usebeamercolor{tag2}\colorbox{bg}{\color{fg}Cluster 2:}}
 \raisebox{-.5\height}{\includegraphics[width=90mm]{"./Figures/Scores1/score1 (no scale) cluster2".png}}

{\usebeamercolor{tag3}\colorbox{bg}{\color{fg}Cluster 3:}}
 \raisebox{-.5\height}{\includegraphics[width=90mm]{"./Figures/Scores1/score1 (no scale) cluster3".png}}
\end{center}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions}
\begin{frame}
\frametitle{Conclusions}

\begin{itemize}
\item \texttt{PCDimension} contains highly accurate, fast methods for
  estimating the number of principal components.
\item \texttt{Thresher} is effective at removing outliers that do not
  contribute to clustering.
\item \texttt{Thresher} provides accurate estimatiion of the number
  of clusters when there are more meaurements than objects to cluster.

\item The AML RPPA data on apoptosis suggest that we can identify
  interpretable one-dimensional ``pathway building blocks'' (PBBs)
  in biological pathways.
\item The TCGA RNA-Seq data on IL4 Signaling suggests that these PBBs
  occupy different states in different kinds of cancer, and may
  potentially serve as biomarkers.
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Acknowledgement}
 \begin{itemize} 
  \item Dr. Steven Kornblau, The University of Texas MD Anderson Cancer
Center.
  \item Dr. Min Wang, The Ohio State University.
  \item Dr. Zachary Abrams, The Ohio State University.
  \item Kristi Bushman, University of Pittsburgh.
  \item[]
  \item Grants: NSF No. 1440386; NIH/NCI P30 CA016058; R01 CA182905,
    NIH/NLM T15LM011270.
 \end{itemize}

\textcolor{osuscarlet}{Availability:} The methods described here are
implemented in R packages called \texttt{PCDimension} and
\texttt{Thresher}. You can get the source code or the latest package
build from R-Forge, or you can download binaries from the repository
at \url{http://silicovore.com/OOMPA}.

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TOC at end ...
\begin{frame}[plain,noframenumbering]
\begin{multicols}{2}
  \tableofcontents
\end{multicols}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
