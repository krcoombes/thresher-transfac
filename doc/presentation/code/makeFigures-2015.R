# analysis of one part of AML RPPA data

# libraries
library(ClassDiscovery)
library(RColorBrewer)
library(Thresher)
# get the cleaned data on newly diagnosed patients
load("Data/cleaned.Rda")
AMLdata <- as.matrix(AMLdata)
if (version != "July2013") stop("Bad version:", version, "\n")
# get the pathway descriptions
load("Data/pathways.rda")
# set up figure directories
if (!file.exists("Figures")) dir.create("Figures")
oldfig <- "Figures/Unclean"
if (!file.exists(oldfig)) dir.create(oldfig)
newfig <- "Figures/Cleaned"
if (!file.exists(newfig)) dir.create(newfig)

# standard figures
names(pathways)
n <- 'apoptosis'
p <- pathways[[n]]
m <- p$members
d <- as.matrix(AMLdata[,m])
thresh0 <- Thresher(d, n, method="auer.gervini")
reap0 <- Reaper(thresh0, useLoadings=TRUE, method="auer.gervini")
#reap1 <- Reaper(thresh0, useLoadings=FALSE, method="auer.gervini")
#makeFigures(reap1)
#table(NO=groups(reap1), YES=groups(reap0))

makeFigures(thresh0, DIR=oldfig)
makeFigures(reap0, DIR=newfig)

tem <- m
names(tem) <- NULL
tem


# shattered
groups <- function(object, K=NULL) {
  if(is.null(K)) {
    K <- object@nGroups
    abcols <- predict(object@fit)
  } else {
    abcols <- cutree(object@gc, k=K)
  }
  gps <- paste("G", abcols, sep='')
  names(gps) <- colnames(object@data)
  gps
}

c1 <- groups(reap0)

for(i in 1:length(unique(c1))) {
  g <- paste("G", i, sep='')
  n1 <- paste(n, g, sep='-')
  w <- which(c1==g)
  m <- names(w)
  d <- as.matrix(AMLdata[,m])
  thresh <- Thresher(d, n1, method="auer.gervini")
  reap <- Reaper(thresh, useLoadings=TRUE, method="auer.gervini")
  
  makeFigures(thresh, DIR=oldfig)
  makeFigures(reap, DIR=newfig)
}
