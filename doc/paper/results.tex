\section*{Results}

\subsection*{Number of Principal Components}
We performed principal components analysis (PCA) on the dataset
containing expression measurements of 486 transcription factors, as
listed in the Transcription Factor Catalog \cite{fulton2009}, in
10,446 samples from studies of 33 different kinds of cancer in The
Cancer Genome Atlas. The numbers of samples per cancer type are listed
in \tref{cancerType}.  In order to estimate the number of significant
components, we used the \texttt{PCDimension} R package
\cite{wang2017}, which implements automatic rules for the graphical
Bayesian method introduced by Auer and Gervini \cite{auer2008}. The
Auer-Gervini model uses a family of exponentially decaying prior
distributions parametrized by a variable called~$\Theta$ that controls
the decay rate; they showed that the maximum a posteriori (MAP)
estimate of the number of components is a non-increasing step function
of~$\Theta$. In \textbf{Supplementary Figure 1}, we have plotted this
step function for the TCGA transcription factor data.

In their paper, Auer and Gervini advise looking at this plot and
selecting the ``highest step that is long''  to define the number of
components. In our paper , we examined a variety of
rules for automating this selection, including
\begin{itemize}
\item ``Twice Mean'', in which any step that is longer than twice the
  mean step length is viewed as long;
\item ``CPT'', in which we first sort the steps by the length in
  increasing order, and then apply the ``At Most One Change''
  algorithm implemented by the \texttt{cpt.mean} function in the
  \texttt{changepoint} R package to detect the first change point; and
\item ``Kmeans3'', in which we apply the K-means algorithm with $K=3$
  to cluster the step lengths into small, medium, and large, where
  both ``medium'' and ``large'' are viewed as long.
\end{itemize}
In the simulation studies \cite{wang2017}, we found that the first two
of these methods, in particular, were competitive with the best
existing techniques to estimate the number of components.  When
applying these methods to the transcription factor data, CPT claims
that there are four components; Kmeans3 claims that there are 18, and
Twice Mean claims that there are 29.

\subsection*{Principal Components Distinguish Cancer Types}

To test visually whether the Twice Mean estimate of 29 significant
principal components is reasonable, we prepared pairwise plots of
different components. Some of these plots are shown in \fref{pca}; a
more extensive set is contained in \textbf{Supplementary Figures 2--15}. In
each plot, samples are colored by cancer type according to the color
scheme shown in the bottom right panel. 
In panel~(A) of \fref{pca}, we show PCs~1 and~2.  The
``jade'' samples in the upper right are low-grade gliomas (LGG). 
%In panel~(B), PCs~3 and~4, the ``reddish orange'' samples at the top are
%acute myeloid leukemia (LAML).  
In panel~(B), PCs~9 and~10, the two
different shades of blue at the lower right come from samples of uveal
or cutaneous skin melanomas (UVM; SKCM). In panel~(C), PCs~13 and~14,
the ``pale yellow'' samples at the top are pheochromocytoma and
paraganglioma (PCPG) cancers.  The ``pale green'' at the bottom are
testicular germ cell tumors (TGCT). In panel~(D), PCs~23 and~24, the
``magenta'' samples at the right are bladder cancer (BLCA) and the
``yellowish green'' at the bottom are sarcomas (SARC). In panel~(E),
PCs~27 and~28, the purple samples at the left are kidney chromophobe
(KICH), one of three types of kidney cancer studied in TCGA, and the
``red'' samples are adrenocortical carcinomas (ACC). The ``turquoise''
samples at the right are thymomas (THYM).  These figures support the
conclusion that the principal components, at last including components
23--28 as claimed by the ``Twice Mean'' algorithm,  contain
information that helps distinguish different cancer types.

\subsection*{Information to Distinguish Most Cancer Types is Present
  in 29 Principal Components}

Linear projections, such as those implemented in PCA, do not always
give an accurate picture of how well-separated subgroups really are in
high-dimensional spaces. In order to obtain more accurate
visualizations, we applied the method of t-distributed stochastic
neighbor embedding (t-SNE) \cite{vandermaaten2008,krijthe2015}.  The results
are shown in \fref{sne}. In this figure, primary tumors are plotted
with an open circle, metastases with a hollow triangle, and normal
samples with an asterisk.  This plot reveals the following results:
\begin{enumerate}
\item In almost every case, samples from one kind of cancer are well
  separated from other kinds.
\item However, colon cancer (COAD) and rectal cancer (READ) are essentially
  indistinguishable.    (See the bottom of the figure, right of center.)
\item Moreover, normal samples of COAD or READ can be distinguished
  from tumors, but not from each other.
\item The two types of lung cancer (LUAD and LUSC, right of
  center) can mostly be distinguished, although there are a few
  samples that unexpectedly overlap the other group.
\item However, all the normal lung samples cluster together.
\item Even though one type of primary kidney cancer (KICH, far right)
  is very unlike the other two types (KIRC and KIRP, to the upper
  right), their normal samples cluster together.
\item There are clearly two very different subtypes of esophogeal
  cancer (ESCA). One clusters with the stomach cancers (STAD; bottom
  center) while the other clusters with the head-and-neck cancers
  (HNSC; center).
\item Most of the time, we can tell normal samples from primary
  tumors. In addition to the lung, kidney, and colorectal cancers that
  we have already mentioned, we can also separate subclusters of
  normal samples for thyroid (THCA; upper left), liver (LIHC, top
  center), prostate (PRAD, right), and breast (BRCA, lower right).
\item Breast cancer is also interesting, in that there are clearly at
  least two well-separated subtypes of breast cancer. The smaller set
  consists of triple negative breast cancer cases.
\end{enumerate}

\subsection*{Finding Biological Components}
In addition to the fact that linear projections in PCA may not reveal
the full extent of the separation of subtypes in high-dimensional
spaces, the components themselves are difficult to interpret
biologically.  Whenever we use genes to cluster samples, the
individual PCs are comprised of weighted linear combinations of genes.
These combinations are chosen to maximize the percentage of variance
explained and to satisfy the mathematically desirable property of
orthogonality. In situations where many different biological processes
may be at work, however, each PC often turns out to combine the
effects of multiple processes.

To address this problem, we applied a new method, Thresher, that we
recently developed \cite{wang2018}. The Thresher algorithm has three
steps:
\begin{enumerate}
\item Use the \texttt{PCDimension} package \cite{wang2017} to
  determine the number \(D\) of significant principal components. Then
  we can view each gene (or transcription factor) as a vector of
  weights in the principal component space of dimension $D$.
\item The magnitude, or length, of these vectors is used to identify
  and remove outliers. Our simulations suggest that vectors of length
  \(<0.3\) are safe to remove~\cite{wang2018}.
\item The remaining genes are then clustered based on the directions of
  their weight vectors. Equivalently, this process converts each gene
  into a point on a hypersphere in PC space.  To cluster such points,
  we model the data using a mixture of von Mises-Fisher distributions
  \cite{banerjee2005,hornik2014}.  We assume that the number $K$ of
  clusters satisfies $D \le K \le 2D$ and use the Akaike Information
  Criterion (AIC) to select the optimal $K$.
\end{enumerate}
We want to emphasize two key points about the last step in this
process. First, we are replacing the mathematical principal
components, which are chosen to satisfy orthogonality, with more
natural directions defined by the actual genes. For this reason, we
refer to these clustered direction-vectors as ``biological
components.'' Second, we allow the number \(K\) of biological
components to be up to twice as large as the number \(D\) of principal
components. The motivation driving this decision is that we want to
separate genes whose expression patterns are negatively
correlated. Such genes point in opposite directions in principal
component space, and so they do not increase the mathematical
dimension of the space.

When we applied Thresher to the TCGA transcription factor data, no
outliers were found, and the mixture model concluded that there were a
total of 30 clusters of transcription factors.  \textbf{Supplementary
  Table~0} lists the transcription factors belonging to each cluster.
We then considered the data from each cluster separately. In each
case, we found that the cluster spanned a one-dimensional principal
component space (\textbf{Supplementary Figures~16--45}). Moreover, the
weights of the cluster members in the first principal component all
had the same sign and were of roughly comparable magnitudes. Thus, we
concluded that we had identified 30 sets (clusters) of transcription
factors that tended to work together across more than 10,000 samples.

\subsection*{Computation Time}
Operations were timed on an Intel\textsuperscript{\textregistered}
i7-3930 CPU at 3.2 GHz running
Windows\textsuperscript{\textregistered} 7 SP1. Performing PCA and
using \texttt{PCDimension} to compute the number of components took 15
seconds. Running t-SNE took 93 seconds. Running \texttt{Thresher} took
256 seconds; however, this measurement includes automatically running
the algorithm twice, once before and once after removing
outliers. Each run also includes running the PCDimension code.

\subsection*{Characterizing Biological Components}
We hypothesized that each transcription factor cluster (or biological
component) implements a single biological process.  We used three
different bioinformatics approaches to test this hypothesis and thus
to annotate the biological entity associated with each biological
component.
\begin{enumerate}
\item We prepared ``bean plots'' \cite{kampstra2008} of the average
  expression of each biological component in the TCGA samples,
  separated and colored by cancer type (\fref{beans.tissue},
  \fref{beans.lethal}, and \textbf{Supplementary Figures 46--75}). 
\item We identified the UniGene cluster corresponding to each
  transcription factor
  \cite{wagner2013,wheeler2007}. We found the tissues listed as ``cDNA sources''
  for the UniGene cluster, and for each biological component,
  recorded the tissues that appeared the maximal number of times. 
\item We computed Pearson correlation coefficients between each of the
  30 biological components and all 20,289 genes measured by RNA
  sequencing in the TCGA samples.  For each biological component, we
  took the list of genes whose absolute correlation was at least 0.5
  and uploaded it to the ToppGene website in order to perform gene set
  analyses \cite{chen2009}. 
\end{enumerate}

A summary of the results of these analyses is shown in
\tref{interp}. More complete results are contained in
\textbf{Supplementary Tables 1--30}. We found that 12/30 (40\%) of the
biological components appeared to be associated with a specific tissue
type.  Four examples of the 12 tissue-specific components are shown
in \fref{beans.tissue}.  The remaining 18/30 (60\%) of the components
were associated with fundamental biological processes, including cell
cycle, angiogenesis, apoptosis, mitochondria, ribosomes, and the
endoplastic reticulum.  Eight of these eighteen biological components
were also associated with ``embryonically lethal'' mouse phenotypes;
four examples of the eight ``embryonically lethal''
biological-process components are illustrated in \fref{beans.lethal}.

