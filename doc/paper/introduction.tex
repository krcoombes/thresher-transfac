%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                          %%
%% The Main Body begins here                %%
%%                                          %%
%% Please refer to the instructions for     %%
%% authors on:                              %%
%% http://www.biomedcentral.com/info/authors%%
%% and include the section headings         %%
%% accordingly for your article type.       %%
%%                                          %%
%% See the Results and Discussion section   %%
%% for details on how to create sub-sections%%
%%                                          %%
%% use \cite{...} to cite references        %%
%%  \cite{koon} and                         %%
%%  \cite{oreg,khar,zvai,xjon,schn,pond}    %%
%%  \nocite{smith,marg,hunn,advi,koha,mouse}%%
%%                                          %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%% start of article main body
% <put your article body there>

%%%%%%%%%%%%%%%%
%% Background %%
%%
\section*{Background}\label{background}
Transcription factors (TF) are proteins that bind to DNA and control
the rate of transcription for a set of genes; they are some of the
most important regulators of gene expression \cite{chen2007}. In
particular, they play a crucial role in development, differentiation, and
the maintenance of cell type \cite{davidson2006}. Furthermore, about
one-third of TFs are tissue-specific \cite{lambert2018}, and TFs are
over-represented among oncogenes \cite{ehsani2016}. Because of the
vital role of TFs in the regulation of multiple critical
biological processes, we hypothesize that the expression patterns of
transcription factors contain sufficient information to distinguish
between different types of cancer.

In order for TFs to carry out their regulatory programs, they must
cooperate by forming networks \cite{chen2007}.  Gaining a better
understanding of how TFs cooperate to regulate gene expression can
help us gain deeper insight into human genetics and disease, especially
cancer. In order to identify cooperating TF networks, some researchers
have clustered TFs according to known function or disease
association \cite{sandelin2004,berman2002}. Others have focused on
clustering TF binding sites by looking for common sequence
motifs \cite{shenorr2002,wei2006}. Still other studies have applied
clustering algorithms to patterns of TF protein expression
\cite{johansson2003,frith2003}. These studies are motivated by the
observation that, essentially by definition, TFs working in concert
must bind (to the same or to nearby binding sites, possibly exhibiting
similar motifs) \textit{at the same time} \cite{reiter2017}.  In other
words, cooperating sets of TFs tend to be expressed together so that TF
coexpression may be an effective proxy for
cooperativity \cite{wang2016, zeidler2016}.  To understand which
TFs cooperate (and thus distinguish tissue types and cancer types), we
propose to cluster them into biologically meaningful sets based on
their coexpression at the mRNA level.

Clustering ``features'' (genes, proteins, transcription factors, etc.)
is a core research problem in biomedical
informatics \cite{bendor1999,eisen1998,weinstein1997}. The ability to
group biological features into distinct \textit{biologically
  interpretable} clusters would solve many important but challenging 
research problems, such as the identification of multi-dimensional
biomarkers.  The challenges posed by these research problems result in
part from the nature of omics research, which has dramatically increased
the feature space in many biomedical domains \cite{holzinger2014}. For
this reason, grouping and clustering problems are more prevalent than
ever and require more creative and robust solutions. In addition, as
researchers increasingly look for more complex patterns in omics data,
ensuring the biological interpretability of results is an increasingly
important task \cite{bellazzi2011}.

In this article, we apply a novel solution to the problem of
clustering transcription factors; \fref{workflow} illustrates the
worflow.  We demonstrate the ability of our
recently described algorithm, Thresher \cite{wang2018}, to cluster
transcription factors into biologically interpretable one-dimensional
clusters.  Thresher employs concepts from principal component
analysis, outlier filtering, and von Mises-Fisher mixture models.  It
is specifically designed both to determine the optimal number of
clusters after filtering out insignificant ''outlier'' features and to
replace the purely mathematical principal components with biologically
relevant and interpretable clusters.  We apply Thresher to the set of
more than 10,000 RNA-Seq gene expression profiles of 33 kinds of
cancers taken from The Cancer Genome Atlas (TCGA) \cite{weinstein2013}.
We show that the expression patterns of 486 transcription factors in
this dataset can be summarized by 29 principal components that are
capable of distinguishing almost all of the cancer types assayed by
TCGA, including separating cancer samples from the adjacent normal
tissue. We further show that the 29 mathematical principal components
can be replaced naturally by 30 clusters, which we call
``\textit{biological components}.''  Each biological component has its
own internal and coherent biological meaning. About 40\% of the
biological components appear to be directly related to a specific
tissue type, while the other 60\% are related to fundamental
biological processes such as the cell cycle, angiogenesis, or
apoptosis. We believe that Thresher's ability to replace principal
components with biologically interpretable components will have broad
applicability.
