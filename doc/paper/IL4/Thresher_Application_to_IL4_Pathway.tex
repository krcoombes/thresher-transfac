%&latex
\documentclass[12pt]{article}
\usepackage{amsmath}
\usepackage{graphicx,psfrag,epsf}
\usepackage{enumerate}
\usepackage{natbib}
\usepackage{url} % not crucial - just used below for the URL 
\usepackage{bm}
\usepackage{mathrsfs, amssymb}
\usepackage{subfig}
\usepackage[]{color}
\usepackage[svgnames]{xcolor}
\usepackage{framed}
\usepackage{alltt}

%\pdfminorversion=4
% NOTE: To produce blinded version, replace "0" with "1" below.
\newcommand{\blind}{0}
\newcommand{\quotes}[1]{``#1''}
%%%%%%%%%%%
% required KNITR definitions
% sample code in appendix was produced by knitr
\newenvironment{knitrout}{}{} % an empty environment to be redefined in TeX
\definecolor{fgcolor}{rgb}{0.345, 0.345, 0.345}
\newcommand{\hlnum}[1]{\textcolor[rgb]{0.686,0.059,0.569}{#1}}%
\newcommand{\hlstr}[1]{\textcolor[rgb]{0.192,0.494,0.8}{#1}}%
\newcommand{\hlcom}[1]{\textcolor[rgb]{0.678,0.584,0.686}{\textit{#1}}}%
\newcommand{\hlopt}[1]{\textcolor[rgb]{0,0,0}{#1}}%
\newcommand{\hlstd}[1]{\textcolor[rgb]{0.345,0.345,0.345}{#1}}%
\newcommand{\hlkwa}[1]{\textcolor[rgb]{0.161,0.373,0.58}{\text{#1}}}%
\newcommand{\hlkwb}[1]{\textcolor[rgb]{0.69,0.353,0.396}{#1}}%
\newcommand{\hlkwc}[1]{\textcolor[rgb]{0.333,0.667,0.333}{#1}}%
\newcommand{\hlkwd}[1]{\textcolor[rgb]{0.737,0.353,0.396}{\text{#1}}}%
\definecolor{shadecolor}{rgb}{.97, .97, .97}
\definecolor{messagecolor}{rgb}{0, 0, 0}
\definecolor{warningcolor}{rgb}{1, 0, 1}
\definecolor{errorcolor}{rgb}{1, 0, 0}

\makeatletter
\newenvironment{kframe}{%
 \def\at@end@of@kframe{}%
 \ifinner\ifhmode%
  \def\at@end@of@kframe{\end{minipage}}%
  \begin{minipage}{\columnwidth}%
 \fi\fi%
 \def\FrameCommand##1{\hskip\@totalleftmargin \hskip-\fboxsep
 \colorbox{shadecolor}{##1}\hskip-\fboxsep
     % There is no \\@totalrightmargin, so:
     \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
 \MakeFramed {\advance\hsize-\width
   \@totalleftmargin\z@ \linewidth\hsize
   \@setminipage}}%
 {\par\unskip\endMakeFramed%
 \at@end@of@kframe}
\makeatother
%%%%%%%%%%%

%%%%%%%%%%%
\def\rcode#1{\texttt{#1}}
\def\rpkg#1{\textbf{#1}}
%%%%%%%%%%%

%%%%%%%%%%%
% change numbering scheme for appendix/supplement
\newcommand{\beginsupplement}{%
        \setcounter{table}{0}
        \renewcommand{\thetable}{S\arabic{table}}%
        \setcounter{figure}{0}
        \renewcommand{\thefigure}{S\arabic{figure}}%
        \setcounter{section}{0}
        \renewcommand{\thesection}{S\arabic{section}} %
     }
%%%%%%%%%%%

\addtolength{\oddsidemargin}{-.75in}%
\addtolength{\evensidemargin}{-.75in}%
\addtolength{\textwidth}{1.5in}%
\addtolength{\textheight}{1.3in}%
\addtolength{\topmargin}{-.8in}%


\begin{document}

\def\spacingset#1{\renewcommand{\baselinestretch}%
{#1}\small\normalsize} \spacingset{1}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\if0\blind
{
  \title{Comprehensive genomic characterization of IL4 signaling pathway via Thresher}
  \author{ \\
\date{}
}
  \maketitle
} \fi

\if1\blind
{
  \bigskip
  \bigskip
  \bigskip
  \begin{center}
    {\LARGE\bf Title}
\end{center}
  \medskip
} \fi

\bigskip
\begin{abstract}
The IL-4 pathway is an important and multifunctional pathway especially when it is 
involved in cancer progression. Understanding the different subcomponents of this 
pathway are critical improve our understanding of how this pathway operates in 
different cancer domains. Thresher is a novel biostatistical method of clustering 
objects of interest that can also detect outliers and define the number of relevant 
clusters. With the Thresher method, it becomes possible to identify noise features 
and select the optimal decomposition of the set of  “good” genes or proteins from 
a pathway into clusters, each of which defines a “pathway building block” (PBB). 
Using extensive public data from The Cancer Genome Atlas (TCGA), we apply our 
approach for analyzing subsets of genes or proteins involved in the Interleukin-4 
(IL-4) signaling pathway to identify one-dimensional components from three 
subtypes of lung tissues. Further, the proposed method is also applied to the 
same IL-4 signaling pathway from a dataset of 18 different tumor types from 
TCGA. We expect the full significance of Thresher will become more evident 
as it is consistently applied across a broader and more varied spectrum of 
biomarker discovery and validation scenarios.
\end{abstract}

\noindent%
{\it Keywords:} Dimension reduction; Thresher; Pathway building block; 
IL4 signaling pathway.
\vfill

\newpage
\spacingset{1.45} % DON'T change the spacing!

\section{Introduction}
\label{Introduction}

Pathway level systems biology analyses are critical to understanding the complexity 
of biological systems with respect to cancer. The vast majority of biological systems 
involved in cancer are not simple one-dimensional elements. Rather, they are 
complicated multifaceted and multidimensional networks that must be analyzed at the 
systems biology level. To that end we utilize the Thresher methodology to break 
down the Interleukin-4 cascade pathway into individual one-dimensional elements 
to better understand the interplay between these IL-4 pathway building blocks to 
form a greater understanding of how the IL-4 pathway becomes disregulated in 
cancer.

Interleukin-4 (IL-4) is a critical cytokine impacting tumor immunology, yet it appears 
to have paradoxical roles, inducing effective immune responses in some studies, 
while serving as a tumor-promoter in others (\citealt{li09}).  IL-4 protects tumor cells 
from apoptosis by inducing a signaling cascade that leads to many complex effects, 
one being the differentiation of Type 2 helper T-cells (Figure \ref{IL4}). Research has 
shown that IL-4 and its receptors are overexpressed in many cancers. 
Overexpression of IL-4 causes the up-regulation of anti-apoptotic genes and 
promotes tumor growth. Consequently, it is essential to understand how 
disregulation of the IL-4 pathway impacts cancer since this will aid in understanding 
how best to target and correct the IL-4 pathway under various cancer scenarios. 
However, given the paradoxical nature of the existing studies, it is critical to be 
able to breakdown the IL-4 pathway into discreet subunits in order to fully 
comprehend the biochemical processes involved. 

Current research indicates that the IL-4 pathway can have opposite effects on 
tumor growth. Animal studies paradoxically indicate IL-4’s potential for tumor
 immunity (\citealt{golumbek91, pericle94}), and tumor growth (\citealt{stremmel99}). 
 While human research has focused on the role of the IL-4 pathway in 
 tumorigenesis. Tumor cells express elevated IL-4R on their surfaces although 
 variation by tumor type is noticeable. Renal cell carcinoma (RCC) and breast 
 cancer cells can reach levels above 4000 binding sites/cell, while head and 
 neck cancer cells evidence over10,000 binding sites/cell, in contrast to normal 
 endothelial cells which have less than 50 binding sites/cell (\citealt{li09}). IL-4 also 
 protects tumor cells from apoptosis via its multiple impacts on B cells and T cells. 
 The difficulty is that studies are contradictory, the pathway is complex with 
 multiple functions, and it specifically influences cell life, consequently, there 
 are currently more questions related to IL-4 pathway’s role in cancer then 
 there are clear answers. The two studies presented here were designed to 
 address two different aspects of IL-4 research. The first is an analysis of 
 two lung cancer subtypes to investigate the differences in the IL-4 pathway 
 between closely related cancers to determine differences in IL-4 pathway 
 dimensionality.  The second study is an analysis of 18 different cancers in 
 TCGA to investigate if similarities in cancerous IL-4 pathways are evident. 

%Literature summary on IL4 signaling pathway. Interleukin-4 (IL4) is a 
%cytokine that induces a signaling cascade that leads to many effects, 
%such as the differentiation of type 2 helper T-cells. IL-4 and its receptor 
%have been shown to be overexpressed in many cancers, including lung 
%cancer. Overexpression of IL-4 causes the up-regulation of anti-apoptotic 
%genes and promotes tumor growth.
%
%Previous analysis in IL4 signaling pathway from tumor data or experiments. From 
%the study on renal cell cancer (RCC) patients in \cite{maeurer95}, RCC tumor cell 
%lines were derived and found to be negative for interleukin-2 (IL-2), IL-4, IL-10 
%and interferon $\gamma$. And by examining the RCC tumor-infiltrating lymphocytes 
%by PCR from patients with primary cell cancer, IL-4 and IL-10 mRNA expressions 
%were demonstrated to exhibit. Furthermore, \cite{wu15} conducted a comprehensive
%meta-analysis, and showed that one polymorphism IL-4-590 C of IL-4 is significantly
%associated with an increased risk of hepatitis C infection and hepatocellular carcinoma, 
%especially among the Asian population.
%
%Briefly describe the method developed and its usage. An innovative statistical 
%method is proposed for clustering objects of interest from generic datasets in 
%biological systems. It combines ideas from an automated Bayesian approach, 
%outlier filtering, and a von Mises-Fisher mixture model, which are the keystones 
%of the three steps in the developed approach. 

This article is structured as follows. The theoretical framework of the proposed 
method is briefly reviewed in Section~\ref{Method}.  In Section~\ref{Lung}, we 
analyze the IL4 signaling pathway in three Lung tissue subtypes from the RNA-seq 
data. In Section~\ref{TCGA},  we assess how the IL-4 pathway is regulated in a 
variety of different cancers to investigate to what extent IL-4 plays similar roles in 
different tumors.  Finally we conclude the paper and summarize the findings and 
remarks in Section~\ref{Conclusions}. 


\section{Method}
\label{Method}

This study employs a novel method for clustering objects of interest from omics 
datasets in biological pathways. Thresher combines elements from (1) an 
automated Bayesian approach in principal component analysis (PCA), (2) 
an outlier filtering process, and (3) von Mises-Fisher mixture models, 
creating a three-step analytic approach. More details on the proposed 
method are described as follows. 

PCA is one of the most common techniques in multivariate analysis for high 
dimensional data. A key difficulty in using PCA is that it relies on the researcher 
to select the number of significant principal components (PCs) that are the 
primary contributors to the variation in the data. In contrast, the Thresher 
method uses an automated computational system within the framework of 
a Bayesian model to calculate the optimal number of PCs from the general 
dataset (\citealt{auergervini, wang16a}). In previous studies 
the most effective clustering approaches were the Broken-Stick model and 
randomization-based procedures. These were compared with the proposed 
Thresher algorithm via a set of extensive simulation studies. The performance 
results relating to accuracy and computational efficiency indicate that the 
automated model with a few specific criteria yielded the most biologically 
relevant clusters across different scenarios. The overall performance result 
of this comparative analysis is consistent under different levels of noise, 
further indicating the robustness of the proposed method.

Thresher is a comprehensive analytic method for the clustering of general 
datasets (\citealt{wang16b}). In addition to defining the optimal number of 
clusters, it also detects the existence of outliers that are often confused with 
the biologically relevant objects of interest. For example, biological features 
such as genes and proteins can be examined and classified as “good” or “noise” 
based on their contribution to the variability in the dataset. Thresher detects 
these “good” objects, and removes the noise or outliers whose distance to the 
origin is less than the default $0.3$ in projected Bayesian principal components 
space. Moreover, the remained “good” objects are then clustered into discrete 
groups based on a von Mises-Fisher mixture model after the structure is 
cleaned up and the optimal number of clusters is chosen based on the minimal
Bayesian information criterion (BIC).  Then, an extensive Monte Carlo simulation 
study with various simulated datasets that cover a wide range of real data is 
conducted to compare the proposed method with the indices in the NbClust 
package . The Thresher method, when applied with criteria TwiceMean and CPT, 
produces the optimal number of clusters (\citealt{charrad14}).


\section{Analysis in Lung Tissue Subtypes}
\label{Lung}

Interleukin-4 (IL-4) is a cytokine that induces a signaling cascade that leads to 
many effects, such as the differentiation of type 2 helper T-cells. IL-4 and its 
receptor have been shown to be overexpressed in many cancers, including lung 
cancer. Overexpression of IL-4 causes the up-regulation of anti-apoptotic genes 
and promotes tumor growth. The purpose of this first study was to create a 
distance metric that could be used to compare gene expression in the IL-4 
pathway between lung adenocarcinoma (LUAD), lung squamous cell carcinoma 
(LUSC), and normal tissues so that variations between aspects of the IL-4 
pathway could be determined.

\begin{figure}
  \centering
    \includegraphics[width=5.2in, height=6.4in]{Figures/IL4.png}
    \caption{Diagram of IL4 signaling pathway from WikiPathways}
    \label{IL4}
\end{figure}

The IL-4 signaling pathway was analyzed from data involving lung tumor-adjacent 
normal tissue and two main types of non-small cell lung cancer (NSCLC) – lung 
squamous cell carcinoma (LUSC) and lung adenocarcinoma (LUAD). LUSC, also 
called epidermoid carcinoma, is the most common type of lung cancer accounting 
for about 30\% of all lung cancers. It begins in the squamous cells -- thin, flat 
cells that look like fish scales when seen under a microscope -- and generally is 
found in the central part of the lung or in one of the main airways (left or right 
bronchus). It is known to be more strongly associated with smoking than any 
other type of non-small cell lung cancer. Comparatively, LUAD accounts for 
roughly 40\% of all lung cancers and usually begins in tissues that lie near the 
outer parts of the lungs (periphery). It may be present for a long time before 
it causes symptoms that lead to its diagnosis. LUAD is often found in never-smokers 
and non-smokers and is the most common type of lung cancer in people under 
the age of 45 and among Asians. 

Tumor adjacent normal tissue is non-tumorous tissue removed during the surgical 
resection of the tumor. Since this non-tumorous tissue is from the same area of 
the lung as the solid tumor, it becomes a valuable control in many tumor normal 
RNA-Seq expression comparative studies.

The RNA-seq data was downloaded from The Cancer Genome Atlas (TCGA) for 
three groups: LUAD, LUSC, and tumor-adjacent normal. The summary of the 
datasets, including the number of samples and the data source, is provided in 
Table \ref{lungtable}. Genes in the IL-4 signaling pathway were clustered using 
Thresher. Genes that are clustered together are likely to be controlled by a 
common regulatory mechanism. Cluster labels were overlaid on the IL-4 signaling 
pathway downloaded from WikiPathways.

A distance metric was created to measure the similarity between graphs. Nodes 
that are further upstream in the pathway are weighted as more important than 
those downstream. Nodes with a high degree of connectivity are also weighted 
as more important.

\begin{table}[htbp]
\centering
\renewcommand{\arraystretch}{1}
\global\long\def\~{\hphantom{0}}
 \caption{Summary of the datasets from three lung tissue subtypes.}{ %
\resizebox{0.75\columnwidth}{!}{
\begin{tabular}{@{\extracolsep{10pt}}lcccc@{}}
\hline
{Subtypes}  & Sample Size  & Source & Important info A &  Important info B \tabularnewline
\hline
LUAD    & $162$ & TCGA & $ 0 $ & $ 0 $ \tabularnewline
LUSC    & $240$ & TCGA & $ 0 $ & $ 0 $  \tabularnewline
Normal & $108$ & TCGA? OSU? & $ 0 $ & $ 0 $ \tabularnewline
\hline
\end{tabular}} 
}
\label{lungtable}
\end{table}

We applied different methods, including the automated Bayesian model, to 
compute the number of significant PCs for the three datasets, and the results 
are provided in Table 2. The number of PCs ranges from 1 to 32 for LUAD and 
1 to 48 for both LUSC and normal tissue. Previous studies (Wang et al. 2016b) 
suggested that the TwiceMean criterion was most effective when the number 
of objects was relatively small. Plots of the maximum posterior estimates of 
the number of components as a function of the prior parameter were made 
for each. From the plots, we can see that they give strong support for d=4 
for LUAD, d=5 for LUSC and d=1 for normal lung tissue data (Figure 
\ref{lungthetaprior}). For each dataset, the genes in the IL-4 signaling pathway 
were projected onto the d-dimensional PC space and the ones that contributed 
least to the data variation were identified as noise genes. The remaining “good” 
genes were clustered based on their directions for each of the three data sets 
respectively, and the Bayesian information criterion selected the optimal number 
of clusters to be 4 for LUAD, 5 for LUSC, and 2 for normal tissue. The clustering 
of the IL-4 pathway genes is displayed in Figure \ref{IL4clusters}.

\begin{table}[htbp]
\centering
%\renewcommand{\arraystretch}{1.5}
%\global\long\def\~{\hphantom{0}}
 \caption{Number of principal components (PCs) from different algorithms on three datasets}{ %
\resizebox{0.9\columnwidth}{!}{
\begin{tabular}{@{\extracolsep{8pt}}lcccccccc@{}}
\hline
 Rules           & \multicolumn{8}{c}{{Auer-Gervini}}\tabularnewline
\cline{2-9}
      & {twicemean}  & {spectral} & {kmeans}  & {kmeans3} & {ttest}  & {ttest2} & {cpt}  & {cpmfun} \tabularnewline
\hline
{LUAD}   & {$4$} & {$4$} & {$1$} & {$1$} & {$4$} & {$4$} & {$1$} & {$18$}\tabularnewline
{LUSC}   & {$5$} & {$5$} & {$1$} & {$1$} & {$48$} & {$5$} & {$1$} & {$35$}\tabularnewline
{Normal}   & {$1$} & {$1$} & {$1$} & {$1$} & {$36$} & {$1$} & {$1$} & {$18$}\tabularnewline
\hline\hline
 Rules           & \multicolumn{3}{c}{{Bartlett}}  & {Broken-Stick}  & \multicolumn{2}{c}{{Rand.-based}}&&\tabularnewline
\cline{2-4} \cline{5-5} \cline{6-7}
      & {bartlett}  & {anderson} & {lawley}  & {broken-stick} & {rnd-Lambda}  & {rnd-F} &&\tabularnewline
\hline
{LUAD}   & {$17$} & {$32$} & {$20$} & {$1$} & {$4$} & {$32$} \tabularnewline
{LUSC}   & {$22$} & {$30$} & {$26$} & {$0$} & {$7$} & {$38$} \tabularnewline
{Normal}   & {$41$} & {$46$} & {$42$} & {$1$} & {$3$} & {$48$} \tabularnewline
\hline
\end{tabular}} 
}
\label{pctable1}
\end{table}

\begin{figure}
     \centering
      \includegraphics[height=0.35\textwidth]{Figures/LUAD_theta_prior.png}\\
      \includegraphics[height=0.35\textwidth]{Figures/LUSC_theta_prior.png}\\
      \includegraphics[height=0.35\textwidth]{Figures/Normal_theta_prior.png}
    \caption{Auer-Gervini step function relating the prior hyperparameter  $\theta$ 
    to the maximum posterior estimate of the number $\hat{d}$ of significant principal 
    components for three dataset. (Top) LUAD; (Middle) LUSC; (Bottom) Normal tissue. }
    \label{lungthetaprior}
\end{figure}

As there are three interpretations of the same pathway, the similarities between 
them were calculated. Since the IL-4 pathway is a cascade pathway with several 
major hub events, two important criteria were employed: (1) how far upstream 
was the gene and (2) what was the conductivity of that gene. Because not all 
genes have the same weight, the focus in this analysis was on the biological 
importance of the gene within the pathway. The equation used to calculate the 
weight of each gene is 
\begin{equation}
\begin{aligned}
\sum c*(a-d)*n*m
\label{eqn1}
\end{aligned}
\end{equation}
where $a$ is one plus the maximum distance from IL-4, $c$ is the percent of 
complex, $d$ is the distance from IL-4, $n$ is the number of adjacent nodes
and
\begin{equation}
\begin{aligned}
\nonumber m = \left \{
        \begin{array}{lll}
           0, ~\text{same clusters;} \\
           1, ~\text{different clusters;} \\
           0.5, ~\text{cluster or not significant.}
           \end{array}
           \right.
\label{defm}
\end{aligned}
\end{equation}

Using this equation we calculated the similarity between all three IL-4 pathway 
maps. The scores for the similarity matrix are shown in Table \ref{similarity}. 
The results indicate that the gene expression patterns in LUSC and normal 
tissue were the most similar (Figure \ref{IL4clusters}) The IL-4 
signaling pathway gene expression is more abnormal in LUAD than in LUSC. 
Differences in the clustering of STAT6, JAK3, IL4, and FES cause LUAD to 
differ greatly from LUSC and normal. 

\begin{table}[htbp]
\centering
\renewcommand{\arraystretch}{1}
\global\long\def\~{\hphantom{0}}
 \caption{Scores for similarity between the three IL-4 pathway maps.}{ %
\resizebox{0.4\columnwidth}{!}{
\begin{tabular}{@{\extracolsep{10pt}}lcccc@{}}
\hline
             & LUAD  & LUSC & Normal \tabularnewline
\hline
LUAD    & 0 & 380.6 & $ 394.7 $ \tabularnewline
LUSC    & 380.6 & 0 & $ 242.6 $  \tabularnewline
Normal & 394.7 & 242.6 & 0\tabularnewline
\hline
\end{tabular}} 
}
\label{similarity}
\end{table}


%Explain the clusters overlaying on the IL4 signaling pathway diagrams. The gene 
%expression patterns in LUSC and normal tissue are the most similar. The IL-4 
%signaling pathway gene expression is more abnormal in LUAD than in LUSC.


\section{Analysis across Tumors on TCGA}
\label{TCGA}

A second study was to assess how the IL-4 pathway is regulated in a variety 
of different cancers to investigate to what extent IL-4 plays similar roles in 
different tumors. To this end we analyzed the RNA-Seq data of 18 different 
cancers from TCGA. These cancer types were selected because they each had 
a sample size greater than 200. The RNA-Seq datasets were downloaded from 
TCGA and all of these data were collected under the design of Illumina Hiseq 
sequencers with rapid mode v2. Then the gene expression levels were estimated 
from the RNA-Seq data using the software package RSEM. 

Rather than compare the clusters within each type of tumor, for this second 
study we were more interested in determining whether there were any coherent 
internal structures or identifiable “building blocks” across the 18 different types 
of cancer. Of further interest was how the patient samples of various cancers 
were distributed within each building block. Utilizing all of the possible methods 
in the PCDimension package, we obtained the number of significant PCs; the 
maximum posterior estimates of the number of components as a function of the 
prior parameter is plotted in Figure \ref{mergethetaprior}. From this figure, 
we see that d=6, which represents a reasonably large step length; the number 
of significant PCs for the merged data was 6.

In the outlier filtering process, there was one gene -- “IL-4” -- in the IL-4 signaling 
pathway that was identified as noise. This is because this is the beginning of the 
cascade event and as such IL-4 is not dependent on any genes within the 
downstream cascade. The remaining 53 genes were then clustered based on 
their directions in the projected 6-dimensional space within the mixture model 
implemented in Thresher, and the minimum Bayesian information criterion was 
achieved when the number of clusters was 6. Consequently, the analysis indicated 
that there was an optimal decomposition of the IL-4 pathway genes that had 
6 building blocks.

In each building block, we defined a comprehensive “score”, which was the average 
expression level of the genes within the specific cluster for each patient sample for 
the 18 types of cancer. These scores for each building block are shown in bean 
plots in Figure \ref{mergescore}. The mean scores of the 18 tumor patient groups 
for the 6 clusters are provided in the heatmap shown in Figure \ref{heatmap}.

Next, an ANOVA with a follow-up Tukey test was performed to test for significance. 
The overall distribution of the scores based on expression levels of genes in clusters 
1 and 5 were the lowest in liver hepatocellular carcinoma (LIHC) in comparison with 
the other tumors (P = 1.76E-11). The genes in clusters 1 and 5 were thus 
under-expressed in LIHC compared to all other 17 cancers. Similarly, the overall 
distribution of expression levels in cluster 2 was the lowest in brain lower grade 
glioma (LGG) in comparison with the other tumors. The overall scores based on 
expression levels of genes in cluster 3 were the highest in LGG, indicating that the 
genes in building block 2 were under-expressed and those in building block 3 were 
over-expressed in LGG.  

\begin{table}[htbp]
\centering
\renewcommand{\arraystretch}{1.1}
\global\long\def\~{\hphantom{0}}
 \caption{Summary of the datasets from 18 types of tumor in TCGA.}{ %
\resizebox{1\columnwidth}{!}{
\begin{tabular}{@{\extracolsep{10pt}}lcccc@{}}
\hline
{Subtypes}  & Abbreviation & Sample Size  \tabularnewline
\hline
Bladder urothelial carcinoma   & BLCA& $427$  \tabularnewline
Breast invasive carcinoma  & BRCA & $1212$   \tabularnewline
Cervical squamous cell carcinoma and endocervical adenocarcinoma & CESC & $309$ \tabularnewline
Colon adenocarcinoma   & COAD & $328$ \tabularnewline
Head and Neck squamous cell carcinoma  & HNSC & $566$ \tabularnewline
Kidney renal clear cell carcinoma  & KIRC & $606$  \tabularnewline
Kidney renal papillary cell carcinoma   & KIRP & $323$ \tabularnewline
Brain lower grade glioma  & LGG & $530$   \tabularnewline
Liver hepatocellular carcinoma  & LIHC & $423$  \tabularnewline
Lung adenocarcinoma  & LUAD & $576$  \tabularnewline
Lung squamous cell carcinoma  & LUSC & $552$  \tabularnewline
Ovarian serous cystadenocarcinoma  & OV & $307$  \tabularnewline
Prostate adenocarcinoma   & PRAD & $550$  \tabularnewline
Sarcoma  & SARC & $265$   \tabularnewline
Skin cutaneous melanoma & SKCM & $473$\tabularnewline
Stomach adenocarcinoma   & STAD & $450$\tabularnewline
Thyroid carcinoma  & THCA & $568$ \tabularnewline
Uterine Corpus Endometrial Carcinoma  & UCEC& $201$\tabularnewline
\hline
\end{tabular}} 
}
\label{tcgatable}
\end{table}

%Instead of comparing the clusters within each type of tumor, we are interested in 
%whether there is any coherent internal structure or blocks among most of the 18 
%different types of tumors, and how the patient samples of various tumors are 
%distributed in the coherent blocks. By utilizing all the possible methods in the 
%PCDimension package, we obtain the numbers of significant PCs. Also the 
%maximum posterior estimates of the number of components as a function of the 
%prior parameter is plotted in Figure~\ref{mergethetaprior}. From this figure, we
%can see that $d=6$ corresponds to a reasonable large step length and the number
%of significant PCs for the merged data is $6$ here.

%%\begin{table}[htbp]
%%\centering
%%%\renewcommand{\arraystretch}{1.5}
%%%\global\long\def\~{\hphantom{0}}
%% \caption{Number of principal components (PCs) from different algorithms on the merged data}{ %
%%\resizebox{0.95\columnwidth}{!}{
%%\begin{tabular}{@{\extracolsep{8pt}}lcccccccc@{}}
%%\hline
%%
%% Rules           & \multicolumn{8}{c}{{Auer-Gervini}}\tabularnewline
%%\cline{2-9}
%%      & {twicemean}  & {spectral} & {kmeans}  & {kmeans3} & {ttest}  & {ttest2} & {cpt}  & {cpmfun} \tabularnewline
%%\hline
%%{PCs}   & {$1$} & {$1$} & {$1$} & {$1$} & {$7$} & {$1$} & {$1$} & {$6$}\tabularnewline
%%\hline\hline
%%
%% Rules           & \multicolumn{3}{c}{{Bartlett}}  & {Broken-Stick}  & \multicolumn{2}{c}{{Rand.-based}}&&\tabularnewline
%%\cline{2-4} \cline{5-5} \cline{6-7}
%%      & {bartlett}  & {anderson} & {lawley}  & {broken-stick} & {rnd-Lambda}  & {rnd-F} &&\tabularnewline
%%\hline
%%{PCs}   & {$53$} & {$53$} & {$53$} & {$1$} & {$9$} & {$53$} \tabularnewline
%%\hline
%%
%%\end{tabular}} 
%%}
%%\label{pctable2}
%%\end{table}

\begin{figure}
  \centering
    \includegraphics[width=4.8in]{Figures/merge_theta_prior_v.png}
    \caption{Auer-Gervini step function relating the prior hyperparameter $\theta$ 
    to the maximum posterior estimate of the number $\hat{d}$ of significant 
    principal components for merged data.}
    \label{mergethetaprior}
\end{figure}

%In the outlier filtering process, there is one gene ``IL4'' in the IL4 signaling pathway 
%that is identified as noise. Explain why it's reasonable to think of it as a noise feature. 
%The rest 53 genes are then clustered based on their directions in the projected 
%$6$-dimensional space with the mixture model implemented in Thresher, and the 
%minimum Bayesian information criterion is achieved when the number of clusters is 
%$6$. That means, there is an optimal decomposition of the IL4 pathway genes 
%which has $6$ building blocks. 
%
%In each building block, we define a comprehensive ``score'', which is the average 
%expression levels of the genes within the specific cluster for each patient sample, 
%for assessment of classification of the $18$ types of tumors. The scores 
%within $4$ clusters (building blocks) for all patient samples of 18 tumors are 
%shown in Figure~\ref{mergescore}. And the mean scores of the 18 tumor patient
%groups for the $7$ clusters are provided in the heatmap of Figure~\ref{heatmap}.

\begin{figure}
     \centering
      \includegraphics[height=0.3\textwidth, width=0.9\textwidth]{Figures/score1_(no_scale)_cluster1.png}\\
      \includegraphics[height=0.3\textwidth, width=0.9\textwidth]{Figures/score1_(no_scale)_cluster2.png}\\
      \includegraphics[height=0.3\textwidth, width=0.9\textwidth]{Figures/score1_(no_scale)_cluster3.png}\\
      \includegraphics[height=0.3\textwidth, width=0.9\textwidth]{Figures/score1_(no_scale)_cluster5.png}
    \caption{Mean scores of the patients within each tumor type for 4 clusters (Top) 
    Cluster 1; (Top Middle) Cluster 2; (Bottom Middle) Cluster 3; (Bottom) Cluster 5. }
    \label{mergescore}
\end{figure}

%Explain the results. The overall distribution of the scores based on expression 
%levels of genes in cluster $1$ and $5$ are the lowest in liver hepatocellular carcinoma 
%(LIHC) by comparing with that in the other tumors. That means the genes in building
%block $1$ and $5$ are under-expressed in LIHC. Similarly, the overall distribution of 
%the scores based on expression levels of genes in cluster $2$ are the lowest  in brain 
%lower grade glioma (LGG) by comparing with that in the other tumors. And the overall 
%scores based on expression levels of genes in cluster $3$ are the highest in LGG.
%That means the genes in building block $2$ are under-expressed, and those in building
%block $3$ are over-expressed in LGG.


\section{Discussion and Conclusion}
\label{Conclusions}

%Conclude that our method is a comprehensive novel method that is performing well
%in clustering. Good at three aspects: (1) computing the dimensionality or number of 
%components in data; (2) identifying good and bad features; (3) choosing optimal 
%number of clusters and decomposition of good features.

We analyzed the IL-4 pathway within the two major subtypes of lung cancer utilizing 
Thresher. The results indicated that there are 4 building blocks in the IL-4 signaling 
pathway for LUAD tumors, 5 building blocks for LUSC tumors, and 2 building blocks for 
tumor-adjacent normal tissues. In addition, the gene expression patterns in LUSC and 
normal tissues are most similar to each other compared to LUAD. This may indicate that 
IL-4 pathway disruption occurs more frequently in LUAD cancer cases than in LUSC. 

In the optimal decomposition of the IL-4 signaling pathway using 18 diferent cancers 
form TCGA, we found that there were 6 building blocks selected from the merged 
dataset of 18 types of cancer. A comprehensive score metric was defined and the 
overall distribution of the scores based on the expression patterns of the genes in 
building blocks indicated that genes in cluster 1 and 5 were under-expressed in LIHC 
and those in cluster 2 were under-expressed in LGG. The genes in building block 3 
were over-expressed in LGG. 

This advances our understanding of which cancers the IL-4 pathway may play a more 
significant role in tumorogenesis. Prior research had indicated that IL-4 plays a 
significant role in liver cancer, a conclusion supported by our results. However since 
this is not the case in every IL-4 building block, this may be an indication of what 
genes within the IL-4 pathway are important in liver cancer. Similarly, Lower Grade 
Glioma and other brain cell cancers have strong associations with IL-4 pathway 
dysregulation. This is of interest since some building blocks are particularly 
under-expressed while other building blocks are over-expressed. This may indicate 
that some functions of the IL-4 pathway are being shut off while other functions may 
be over-expressed in LGG tumors. Since the IL-4 pathway has multiple functions, some 
of which are pro-apoptotic and some of which are protective against apoptosis, we 
hypothesize that in these particular tumors the cell protective aspects of the IL-4 
pathway are over-expressed and the pro-apoptotic aspects are down-regulated. 
Further research is necessary in LGG tumors and IL-4 but we believe that this 
research presents evidence to the importance of IL-4  pathway regulation in LGG 
tumor progression.

The important methodological element to this research is the subdivision of a 
multidimensional biological pathway into its one-dimensional subcomponents via 
Thresher. This is important because analyzing a multi-dimensional pathway as being 
either “on” or “off” is not really an appropriate way to study systems biology. By 
breaking down the IL-4 pathway into one-dimensional subcomponents, or building 
blocks, we alter the level of analysis to more biologically appropriate one-dimensional 
units. This enables a finer level of analysis, as it is clear that pathways are not binarily 
“on” or “off” but rather their building blocks collectively form an interactive mosaic 
that reveals much more about the overall functioning of the pathway. In the future 
we hope to use this methodology to study other complex biological pathways and 
their individual building block relationship to cancer biology. This methodology could 
easily be applied to other critical pathways, or to more biologically specific questions, 
for example, if the IL-4 pathway changes with cancer stage progression in the same 
cancer. Future research in this area may provide valuable answers about the 
paradoxical nature IL-4 in cancer. 

%Review the findings that we have for analyzing the IL4 pathway in three subtypes
%of lung tissue. There are $4$ building blocks in IL4 signaling pathway for LUAD
%tumors, $5$ building blocks for LUSC tumors, and $2$ building blocks for tumor-adjacent
%normal tissues. Also the gene expression patterns in LUSC and normal tissues are
%most similar. And the IL-4 signaling pathway gene expression pattern is more 
%abnormal in LUAD than in LUSC.

%Review the interesting results that we obtain in the analysis of $18$ different 
%tumor types on TCGA. In the optimal decomposition of the IL4 signaling pathway
%genes, there are $6$ building blocks selected from the merged dataset of $18$ 
%types of tumor. A comprehensive score metric is defined and the overall distribution 
%of the scores based on the expression pattern of the genes in building blocks (clusters)
%suggest that genes in cluster $1$ and $5$ are under-expressed in LIHC, and these in
%cluster $2$ in LGG as well. The genes in building block $3$ are over-expressed in LGG.
%
%One possible future direction is the automatic literature search to check the
%consistency between literature findings and analytical results obtained from 
%data.
%
%An alternative possible direction is to use the same application of the analysis 
%procedure for other known signaling pathways which seem to play important 
%roles in cancer.


%\section*{Acknowledgement}
%The authors would like to thank Steven Kornblau for asking many of the
%right questions. This project is supported by NIH/NCI grants
%P30 CA016058 and R01 CA182905.


\bibliographystyle{Chicago}

\bibliography{IL4_reference}


\bigskip
\beginsupplement

\section{APPENDIX: SUPPLEMENTARY MATERIAL}

\begin{figure}
    \begin{tabular}{ccc}
      \includegraphics[width=0.3\textwidth, height=0.65\textwidth]{Figures/NORMAL_IL4_dense.png}\quad
      \includegraphics[width=0.3\textwidth, height=0.65\textwidth]{Figures/LUSC_IL4_dense.png}\quad
      \includegraphics[width=0.3\textwidth, height=0.65\textwidth]{Figures/LUAD_IL4_dense.png}
    \end{tabular}
    \caption{Clustered elements of IL4 signaling pathway in 3 subtypes of lung tissue.}
    \label{IL4clusters}
\end{figure}

\begin{figure}
  \centering
    \includegraphics[height=5.4in]{Figures/heatmap_score1_redgreen.png}
    \caption{Heatmap of mean scores of the clusters for 18 tumor types}
    \label{heatmap}
\end{figure}




\end{document}



