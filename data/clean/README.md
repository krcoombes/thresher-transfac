---
title: "Data Storage"
quthor: "Kevin R. Coombes"
date: "2017-10-23"
---

This folder should, in principle, hold the cleaned data for this
project. The primary raw data source is the RNA sequencing data for 33
kinds of cancer; copies of these files can be found at `../raw`. These
were processed by an R script (`00-prepData.R`), a subset of data for
transcription factors was slected, and the results were stored as
binary R data files in the `RDA` subdirectory. These files are **not**
under version control (because they are binary, large, and unchanging
(more or less)).

Other data sources included here are

+ `HumanTF.csv` is a cleaned version of the transcription factor gene
  list downloaded from the TFCat database.
+ `cancerAbbrev.csv` contains the mapping between the "standard" TCGA
  abbreviatiosn for the type of cancer and the full disease
  name. These were obtained by copying and pasting the table from the
  [TCGA Study Abbreviations](https://gdc.cancer.gov/resources-tcga-users/tcga-code-tables/tcga-study-abbreviations)
  web site at the Genomic Data Commons on 2017-10-23.
+ The folder `RDA` contains cleaned, binary R data versiosn of the
  transcription factor expression profiles from the TCGA.
