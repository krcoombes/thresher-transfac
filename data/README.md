---
Title: "Data Storage"
Author: "Kevin R. Coombes"
Date: 2018-01-22
---

In theory, this should be the data directory for this project. Raw
data (in the form acquired from a primary source) should be stored in
the `raw` subdirectory.  Processed data (typically by manipulating
some raw data) should be stored in the `clean` subdirectory. When the
primary data is in a form that can be used directly without
transformation, it may, nevertheless, be placed in the `clean`
subdirectory.

In practice, however, we have adopted a different system. Both the
data and the "scratch" space can now be located at different locations
on every machine. The idea is to create a JSON file (named
`cytangle.json`) that is stored in `$HOME/Paths` on the local machine.
The script `../code/00-paths.R` reads this JSON file and stores its
contents in an R object (names `paths`, of course).  This system
allows all other code to rely on the `paths` object to find data and
scratch space. The system supports portab ility, which not filling up
the gitlab project with binary files that are not or should not be under
version control
