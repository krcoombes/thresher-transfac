To install the latest (development) versionof a package from inside R, you can use

library(devtools)
install_gitlab("krcombes/thresher", "pkg/PCDimension")
install_gitlab("krcombes/thresher", "pkg/Thresher")
