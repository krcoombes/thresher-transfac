---
Title: "Scratch Space"
Author: "Kevin R. Coombes"
Date: 2018-01-22
---

Both the data and the "scratch" space can now be located at different
locations on every machine. The idea is to create a JSON file (named
`cytangle.json`) that is stored in `$HOME/Paths` on the local machine.
The script `../code/00-paths.R` reads this JSON file and stores its
contents in an R object (names `paths`, of course).  This system
allows all other code to rely on the `paths` object to find data and
scratch space. The system supports portab ility, which not filling up
the gitlab project with binary files that are not or should not be
under version control
