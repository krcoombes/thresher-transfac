---
Title: Source Code
Author: Kevin R. Coombes
Date: 2017-10-23
---

# Source Code
This directory contains the source code to analyze the TCGA RNA
sequencing data on transcription factors. Here are basic
instructions on the order in which scripts should be run.

1. Run the perl script `01-transfac.pl`. This converts the source file
   `../data/raw/tfcat-list.txt` into `../data/raw/refill.txt`.
2. Run the R script `02-transfac.R`. This selects those genes with
   "strong" evidence of being a transcription factor, and finds the
   human annotations that match the mouse identifiers in the
   database. The result is stored in `../data/clean/HumanTF.csv`.
3. Run the R script `100-prepData.R`. This extracts the transcription
   factor data from the TCGA tab-separated-value source files (in
   `../data/raw/TCGA-TNASeq` and saves the results in binary R data
   format (in `../data/clean/RDA`).
4. Run the Rmarkdown script `101-studyTF.Rmd`. This script handles the
   basic analysis tasks: it clusters trancsription factors (into 30
   groups!) and produces a large collection of plots to show that they
   behave sensibly (and interestingly?). Key intermediate results are
   saved as `../scratch/101-intermediate.Rda`.
5. Note that there is no script `102-...`, even though you might have
   expected one.
5. Run the Rmarkdown script `103-OddsNEvens.Rnd`. This script fits linear
   models to test the extent to which the 30 "biological components"
   of transcription factors can predict the expression of arbitrary
   genes. In order to run this for all genes at once, you need a
   computer with more RAM than my laptop holds. **Note:** This has
   probably been superceded by the file sin the 200-range.
6. Run the R script `104-unigene.R` to attach UniGene IDs to the
   Entrez Gene IDs used in the transcription factor data.
7. Only if absolutely necessary, run the perl script
   `105-unigene.pl`. This script queries the NCBI UniGene database to
   get the cDNA sources associated with each transcription factor
   UniGene ID. (It does this reasonably politely, with a five-second
   delay between queries.)
8. Run the Rmarkdown script `106-tissue.Rmd`. This script uses the
   UniGene annotations to determine which tissue types are most
   frequently associated with each biological component (BC) or
   cluster of transcription factors.
9. Do **not** run the script `107-singleBC.Rmd`.  This approach is
   currently deprecated and is likely to be deleted.
10. Run the Rmarkdown screipt `108-correl.Rmd`. This script comnputes
    the Pearson correlation coefficient between each gene and each
    "biological cluster" (that is, an unweighted average of clustered
    transcription factors).
11. At this point, we manually uploaded the gene lists to ToppGene and
    manually downloaded and saved the results.
12. Run the R script `109-combine.R` to merge the results from the
    UniGene analysis (script 106) and the ToppGene analysis (script
    108).
13. Run the Rmarkdown script `110-tsne.Rmd` to perform the t-SNE analysis.
14. Run the perl script `111-makeSuppTable.pl` to create a single
    Excel workbook that contains a separate separate worksheet for
    each cluster. These worksheets contain the top ten entries in each
    category in the ToppGene analysis.


# Second Paper
We have started on the analyses needed for the second paper; the code
for this part should all have numbers in the 200's.

1. For now, at least, you can also ignore the Rmarkdown script
    `200-TF.Rmd`. While not (yet?) depreecated, it's an irrelevant
    side path.
2. Start with the Rmarkdown script `201-linearModels.Rmnd`, which
   tries to explain the mRNA expression levels of all individual genes
   using a linear model with the 30 biologicla components as
   predictors.
