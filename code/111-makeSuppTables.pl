#! perl -w

use strict;
use warnings;
use Excel::Writer::XLSX;

my $dir = "../results/toppgene";
die "Cannot find ToppGene results!\n" unless (-d $dir);

my $wbName = '../results/SuppTables.xlsx';
my $workbook = Excel::Writer::XLSX->new($wbName);

my $MAGIC = 30;      # number of clusters
my $maxEntries = 10; # maximum number of entries per category within a cluster

foreach my $I (1..$MAGIC) {
    my $inFile = "$dir/cluster$I.txt";
    open(SRC, "<$inFile") or die "Unable to open '$inFile': $!\n";
    my $wsName = "Supplementary Table $I";
    my $worksheet = $workbook->add_worksheet($wsName);

    my $line = <SRC>; # this is always the header line
    my @fields = split /\t/, $line;
    $worksheet->write_row(0, 0, \@fields);
    my $currentCategory = "NONE";
    my $records = 0;
    my $nlines = 0;
    while ($line = <SRC>) {
	my @fields = split /\t/, $line;
	my $category = $fields[0];
	if ($category ne $currentCategory) {
	    $records = 1;
	    $currentCategory = $category;
	} else {
	    ++$records;
	}
	unless ($records > $maxEntries) {
	    ++$nlines;
	    $worksheet->write_row($nlines, 0, \@fields);
	}
    }
    close(SRC);
}

exit;
__END__

