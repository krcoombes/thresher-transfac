
#################################################################################
## load the libraries

library(PCDimension)
library(nFactors)
library(Thresher)
library(ClassDiscovery)
library(RColorBrewer)
library(colorspace)
library(changepoint)
library(cpm)
library(movMF)
library(ade4)
library(xlsx)
library(beanplot)
library(randomForest)
library(caret)

dir <- getwd()
rdir <- dirname(getwd())
# rdir <- "C:/Users/wang.1807/Documents"
if (!file.exists("Results2")) dir.create("Results2")
tumorlist <- c("ACC", "BLCA", "BRCA", "CESC", "CHOL", "COAD", 
  "DLBC", "ESCA", "GBM", "HNSC", "KICH", "KIRC", "KIRP", "LAML",
  "LGG", "LIHC", "LUAD", "LUSC", "MESO", "OV", "PAAD", "PCPG", "PRAD",
  "READ", "SARC", "SKCM", "STAD", "TGCT", "THCA", "THYM", "UCEC",
  "UCS", "UVM")
tumorda <- paste("Rda/", tumorlist, ".Rda", sep='')
filelist <- list.files(paste(rdir, "/Data/Cancers_v2", sep=''))
filelist <- filelist[-2*(1:(length(filelist)/2))]
# filelist <- list.files(paste(rdir, "/Cancers_v2", sep=''))
filelist <- filelist[-c(7, 11, 14, 31)]


#################################################################################
## Load data in IL4 signaling pathway from TCGA RNA-seq cohort

for (k in 1:length(tumorlist)) {

if (file.exists(tumorda[k])) {

  load(tumorda[k])

} else {

  # read IL4 signaling pathway genes
  genetab <- read.table(paste(dir, "/GeneCards2EntrezGene.txt", sep=''), 
     header=FALSE, skip=1, na.strings=c("NA",""), sep = "\t")

  IL4 <- read.xlsx(paste(dir, "/pathway genes.xlsx", sep=''), sheetIndex=3, 
      header=FALSE)

  colnames(IL4) <- "ID"
  IL4$gene <- sapply(IL4$ID, function(x) {genetab[which(genetab[,3]==x),1]})

  # read rna-seq data
  rnadata <- read.table(paste(rdir, "/Cancers_v2/", filelist[k],"/", tumorlist[k], 
      ".txt", sep=''), header=TRUE, fill=TRUE) 

  # play with data
  dim(rnadata)
  # rnadata[1:20,1]
  # strsplit(paste(rnadata[2,1]),'[|][[:space:]]*')[[1]][1]=="?"
  # strsplit(paste(rnadata[31,1]),'[|][[:space:]]*')[[1]][1]=="?"

  Truegene <- unlist(lapply(paste(rnadata[,1]), function(x)  {
    strsplit(x,'[|][[:space:]]*')[[1]][1]!="?" && 
    strsplit(x,'[|][[:space:]]*')[[1]][1] %in% IL4$gene
  }))

  generow <- (1:nrow(rnadata))[Truegene]
  length(unique(paste(IL4$gene))) 
  length(generow) 

  rnadata1 <- rnadata[generow,]

  for (i in 2:ncol(rnadata1)) {
    rnadata1[,i] <- as.numeric(rnadata1[,i])
  }

  genename1 <- unlist(lapply(paste(rnadata1[,1]), function(x)  {
      strsplit(x,'[|][[:space:]]*')[[1]][1] 
  }))

  # setdiff(paste(IL4$gene), genename1)

  rownames(rnadata1) <- genename1
  rnadata1 <- rnadata1[sort(genename1),]

  rnadata1t <- t(rnadata1[, -c(1,ncol(rnadata1))])

  # ls()
  save(rdir,dir,IL4,genename1,genetab,rnadata1t,file=tumorda[k])
  rm(genetab, IL4, rnadata, Truegene, generow, rnadata1, rnadata1t)
  gc()

}

}


#############################################################################
### Merged Data

ptnum <- rep(0, length(tumorlist))
for (i in 1:length(tumorlist)) {
  load(tumorda[[i]])
  ptnum[i] <- nrow(rnadata1t)
}

ptnum
tumorlist[which(ptnum < 200)]
ptnum[which(ptnum < 200)]
sum(ptnum)
# 10446
cumsum(ptnum)

tumorlist2 <- tumorlist[which(ptnum > 200)]
tumorda2 <- paste("Rda/", tumorlist2, ".Rda", sep='')
for (k in 1:length(tumorlist2)) {
  sdir <- paste("Results2/", tumorlist2[k], sep='')
  if (!file.exists(sdir)) dir.create(sdir)
}

mergedat <- NULL
ptnum2 <- rep(0, length(tumorda2))
for (i in 1:length(tumorda2)) {
  load(tumorda2[[i]])
  mergedat <- rbind(mergedat, rnadata1t)
  ptnum2[i] <- nrow(rnadata1t)
}
sum(ptnum2)
cumsum(ptnum2)


#############################################################################
### Analysis of IL4 Signaling Pathway on Merged Data
## analyze the dimensionality of the rna-seq data

# some samples have same row name "REF"
refind <- which(rownames(mergedat)=="REF")
rownames(mergedat)[refind] <- paste("REF", 1:length(refind), sep='')

f <- makeAgCpmFun("Exponential")

agfuns <- list(twice=agDimTwiceMean, specc=agDimSpectral,
               km=agDimKmeans, km3=agDimKmeans3, 
               tt=agDimTtest, tt2=agDimTtest2,
               cpt=agDimCPT, cpm=f)

nBartlett(data.frame(scale(mergedat)), nrow(mergedat)) 
# bartlett anderson   lawley 
#       53       53       53

spca1 <- SamplePCA(t(scale(mergedat)))
lam1 <- spca1@variances[1:(ncol(mergedat)-1)] 
obj1 <- AuerGervini(spca1)
bsDimension(lam1) 
# 1

rndLambdaF(scale(mergedat))
# rndLambda      rndF 
#         9        53

compareAgDimMethods(obj1, agfuns) 
# twice specc    km   km3    tt   tt2   cpt   cpm 
#     1     1     1     1     7     1     1     6
plot(obj1, agfuns)
plot(obj1, agfuns, ylim = c(1, 30))
abline(h = 6, lty = 2, col = 2)

## Use Thresher to remove outliers and cluster the features

thresh1a <- Thresher(mergedat, method="auer.gervini", scale=TRUE, agfun=f)
thresh1a@pcdim
colnames(mergedat)[thresh1a@delta<=0.25] 
reaper1a <- Reaper(thresh1a, useLoadings=TRUE, cutoff=0.25, method="auer.gervini")
reaper1a@nGroups # 6
reaper1a@bic 
#       NC=6       NC=7       NC=8       NC=9      NC=10      NC=11      NC=13 
# -27.770596 -21.000157 -19.322968  -8.057934   7.756455  28.951482  88.118850

plot(thresh1a)
X11()
plot(reaper1a)

summary(reaper1a@fit)
fit1 <- apply(reaper1a@fit$P, 1, which.max)
for (i in 1:reaper1a@nGroups) {
  cat("Cluster", i,":\n")
  print(sort(rownames(reaper1a@fit$P)[fit1==i]))
}

clust1 <- rbind(data.frame(RNA=colnames(mergedat)[thresh1a@delta<=0.25], cluster=rep(0,
  sum(thresh1a@delta<=0.25))), data.frame(RNA=rownames(reaper1a@fit$P), cluster=as.numeric(fit1)))
clust1$RNA <- as.character(clust1$RNA)
clust1 <- clust1[order(clust1[,1]), ]

write.table(clust1, "Results2\\Merge_IL4_clusters_2(CPM).csv", sep=",", col.names=NA, 
     row.names=TRUE)

save.image("merge2.Rda")


##############################################################################
### apply Thresher for each type of tumor

for (k in 1:length(tumorlist2)) {

sink(paste("Results2/", tumorlist2[k], "/", tumorlist2[k], ".txt", sep=''))

load(tumorda2[k])

# Bartlett method
try(nBartlett(data.frame(scale(rnadata1t)), nrow(rnadata1t)), silent=FALSE)

# Broken-Stick method
spcak <- SamplePCA(t(scale(rnadata1t)))
lamk <- spcak@variances[1:(ncol(rnadata1t)-1)]
objk <- AuerGervini(spcak)
try(bsDimension(lamk),silent=FALSE)

# Randomization based method
rndLambdaF(scale(rnadata1t))

# Auer-Gervini method
compareAgDimMethods(objk, agfuns)

png(file=paste("Results2/", tumorlist2[k], "/", tumorlist2[k], ".png", sep=''), pointsize = 18,
    width=2*512, height=1.5*512)
plot(objk, agfuns)
dev.off()


## Use Thresher to remove outliers and cluster the features (default TwiceMean)

threshk <- Thresher(rnadata1t, method="auer.gervini", scale=TRUE, agfun=agDimTwiceMean)
threshk@pcdim
colnames(rnadata1t)[threshk@delta<=0.3]
reaperk <- Reaper(threshk, useLoadings=TRUE, method="auer.gervini")
reaperk@nGroups
reaperk@bic

summary(reaperk@fit)
fitk <- apply(reaperk@fit$P, 1, which.max)
for (i in 1:reaperk@nGroups) {
  cat("Cluster", i, ":\n")
  print(sort(rownames(reaperk@fit$P)[fitk==i]))
}

clustk <- rbind(data.frame(RNA=colnames(rnadata1t)[threshk@delta<=0.3], cluster=rep(0,
  sum(threshk@delta<=0.3))), data.frame(RNA=rownames(reaperk@fit$P), cluster=as.numeric(fitk)))
clustk$RNA <- as.character(clustk$RNA)
clustk <- clustk[order(clustk[,1]), ]

write.table(clustk, paste("Results2/",tumorlist2[k],"/",tumorlist2[k], "_IL4_clusters(TwiceMean).csv", 
    sep=''), sep=",", col.names=NA, row.names=TRUE)

sink()

}


##############################################################################
### build gpml diagrams for the clusters

# load packages
require("XML")

# read gpml file

# color <- c("ffffff", "0000cc", "ff0000",  "ffff00", "999999", "ffcc00", 
#      "009933", "00ffff", "660066", "0000ff")
color <- c("ffffff", "00ffff", "ff0000",  "ffff00", "999999", "ffcc00", 
      "009933", "660066", "0000cc", "0000ff")
# ffffff-blank, 0000ff-blue, ff0000-red, ffff00-yellow, 999999-grey, 
# 800000-maroon, 009933-green, 00ffff-aqua, 660066-approx purple,
# 008080-teal, 808000-olive

IL4 <- xmlTreeParse("IL4.gpml", useInternal=TRUE)
ind1 <- as.numeric(which(names(xmlRoot(IL4))=="DataNode"))

for (i in ind1) {
    genei <- xmlAttrs(xmlChildren(xmlRoot(IL4))[[i]])[["TextLabel"]]
    genei <- gsub("[[:space:]]", "", genei)
    if (genei %in% paste(clust1$RNA)) {
      indi <- which(paste(clust1$RNA)==genei)
      colori <- color[clust1[indi,2]+1]
      addAttributes(xmlChildren(xmlChildren(xmlRoot(IL4))[[i]])$Graphics, 
        "FillColor"=colori)
    }
}

saveXML(IL4, "Merge_IL4_2_CPM.gpml")


############################################################################################
### Principal component analysis on merged data

library(digest)
library(devtools)
install_github("ggbiplot", "vqv") 
library(ggbiplot)

mergedat2 <- mergedat
mergedat2 <- cbind(mergedat2, type=rep(0, nrow(mergedat2)))
tcumsum2 <- cumsum(ptnum2)
tcumsum2 <- c(0, tcumsum2)
mergedat2 <- data.frame(mergedat2)

for (i in 1:length(ptnum2)) {
  mergedat2$type[(tcumsum2[i]+1):tcumsum2[i+1]] <- tumorlist2[i]
}

merge.pca <- prcomp(mergedat2[,-ncol(mergedat2)], center=TRUE, scale=TRUE) 
plot(merge.pca, type = "l")
g <- ggbiplot(merge.pca, obs.scale=1, var.scale=1, 
              groups=mergedat2$type, ellipse=TRUE, 
              circle=TRUE)
g <- g + scale_color_discrete(name='')
g <- g + theme(legend.direction='horizontal', 
               legend.position='top')
print(g)

for (i in 1:(length(tumorda2)-1)) {
  load(tumorda2[[i]])
  dat1 <- rnadata1t
  dat1 <- data.frame(dat1)
  dat1$type <- tumorlist2[i]
  for (j in (i+1):length(tumorda2)) {
    load(tumorda2[[j]])
    dat2 <- rnadata1t
    dat2 <- data.frame(dat2)
    dat2$type <- tumorlist2[j]
    merge2dat <- rbind(dat1, dat2)

    merge2.pca <- prcomp(merge2dat[,-ncol(merge2dat)], center=TRUE, scale=TRUE) 
    g <- ggbiplot(merge2.pca, obs.scale=1, var.scale=1, groups=merge2dat$type, 
              ellipse=TRUE, circle=TRUE)
    g <- g + scale_color_discrete(name='')
    g <- g + theme(legend.direction='horizontal', 
               legend.position='top')
    png(file=paste("Fig2/", tumorlist2[i], "_", tumorlist2[j], "_PCA.png", sep=''))
    print(g)
    dev.off()
  }
}

save.image("merge2.Rda")


## investigate the projections of sample points in pooled data

library(Polychrome)
colv <- createPalette(18, seed=c("#aaaaaa", "#FA8E0D", "#682A93"), range=c(30, 70))
colcol <- rep(0, nrow(mergedat2))
for (i in 1:length(ptnum2)) {
  colcol[(tcumsum2[i]+1):tcumsum2[i+1]] <- colv[i]
}

fit <- prcomp(mergedat2[,-ncol(mergedat2)], center=TRUE, scale=TRUE)

# projection of samples on PC space

plot(fit$x[,1], fit$x[,2], xlab="PC1", ylab="PC2", cex=0.5, type="p", pch=19,
    col=as.numeric(as.factor(mergedat2[,ncol(mergedat2)])))

for (p in 1:thresh1a@pcdim) {
  png(file=paste("PCA2/PC", p, ".png", sep=''), pointsize = 18, width=2*512, 
    height=1.5*512)
  plot(fit$x[,p], xlab="Sample index", ylab=paste("PC ",p, sep=''), cex=0.5, 
    type="p", pch=19, col=colcol)
  dev.off()
}

hist(fit$x[,1], breaks=100)

for (p1 in 1:(thresh1a@pcdim-1)) {
  for (p2 in (p1+1):thresh1a@pcdim) {
    png(file=paste("PCA2/PC", p1, "&", p2, ".png", sep=''), pointsize=18, width=2*512, 
      height=1.5*512)
    plot(fit$x[,p1], fit$x[,p2], xlab=paste("PC ",p1, sep=''), ylab=paste("PC ",p2, sep=''), 
      cex=0.5, type="p", pch=19, col=colcol)
    dev.off()
  }
}

plot(0,xaxt='n',yaxt='n',bty='n',pch='',ylab='',xlab='')
legend("center", tumorlist2, col=colv, cex=1.2, pch=19)

mat <- matrix(c(1,2), 1, 2, byrow=TRUE)
layout(mat, widths=c(6, 1), heights=4)
par(mar=c(5, 5, 5, 0.5))
plot(fit$x[,p], xlab="Sample index", ylab=paste("PC ",p, sep=''), cex=0.5, 
    type="p", pch=19, col=colcol)
par(mar=c(0.1, 0.5, 0.1, 0.5))
plot(0, xaxt='n', yaxt='n', bty='n', pch='', ylab='', xlab='')
legend("center", tumorlist2, col=colv, cex=1.1, pch=19)

mat <- matrix(c(1,2), 1, 2, byrow=TRUE)
for (p in 1:thresh1a@pcdim) {
  png(file=paste("PCA2/PC", p, ".png", sep=''), pointsize=18, width=3*512, 
    height=1.5*512)
  layout(mat, widths=c(6, 1), heights=4)
  par(mar=c(5, 5, 5, 0.5))
  plot(fit$x[,p], xlab="Sample index", ylab=paste("PC ",p, sep=''), cex=0.5, 
    type="p", pch=19, col=colcol)
  par(mar=c(0.1, 0.5, 0.1, 0.5))
  plot(0,xaxt='n',yaxt='n',bty='n',pch='',ylab='',xlab='')
  legend("center", tumorlist2, col=colv, cex=1.1, pch=19)
  dev.off()
}

for (p1 in 1:(thresh1a@pcdim-1)) {
  for (p2 in (p1+1):thresh1a@pcdim) {
    png(file=paste("PCA2/PC", p1, "&", p2, ".png", sep=''), pointsize=18, width=2*512, 
      height=1.5*512)
    layout(mat, widths=c(6, 1), heights=4)
    par(mar=c(5, 5, 5, 0.5))
    plot(fit$x[, p1], fit$x[, p2], xlab=paste("PC ", p1, sep=''), ylab=paste("PC ", p2, sep=''), 
      cex=0.5, type="p", pch=19, col=colcol)
    par(mar=c(0.1, 0.5, 0.1, 0.5))
    plot(0, xaxt='n', yaxt='n', bty='n', pch='', ylab='', xlab='')
    legend("center", tumorlist2, col=colv, cex=1.1, pch=19)
    dev.off()
  }
}


## look at two "scores" on distinguishing the tumors

for (i in 1:reaper1a@nGroups) {
  cat("Cluster", i,":\n")
  print(sort(rownames(reaper1a@fit$P)[fit1==i]))
}

# score with equal weights for genes in each cluster
score1.mat <- matrix(0, nrow(mergedat), reaper1a@nGroups)
score1.std.mat <- matrix(0, nrow(mergedat), reaper1a@nGroups)
for (i in 1:reaper1a@nGroups) {
  clustinm <- sort(rownames(reaper1a@fit$P)[fit1 == i])
  score1.mat[, i] <- apply(mergedat[, clustinm], 1, mean) # unscaled data
  score1.std.mat[, i] <- apply(scale(mergedat[, clustinm]), 1, mean) # scaled data
}

colist <- list()
for (s in 1:length(colv)) {
  colist[[s]] <- colv[s]
}

for (p in 1:reaper1a@nGroups) {
  png(file=paste("PCA2/Scores/score1 (no scale) cluster", p, ".png", sep=''), 
    pointsize=16, width=5*512, height=1.5*512)
  # boxplot(score1.mat[, p] ~ mergedat2$type, xlab="Disease", ylab="Score", 
  #   col=colv)
  beanplot(score1.mat[, p] ~ mergedat2$type, xlab="Disease", ylab="Score", 
    col=colist, what=c(1,1,1,0))
  dev.off()
}

png(file=paste("PCA2/Scores/score1 (no scale) all clusters.png", sep=''), 
  pointsize=16, width=5*512, height=3.5*512)
par(mfrow=c(6, 1))
par(mar=c(2.5, 4, 1.5, 2))
for (p in 1:reaper1a@nGroups) {
  beanplot(score1.mat[, p] ~ mergedat2$type, xlab="Disease", ylab="Score", 
    col=colist, what=c(1,1,1,0))
}
dev.off()

for (p in 1:reaper1a@nGroups) {
  png(file=paste("PCA2/Scores/score1 (scaled) cluster", p, ".png", sep=''), 
    pointsize=16, width=5*512, height=1.5*512)
  beanplot(score1.std.mat[,p] ~ mergedat2$type, xlab="Disease", ylab="Score",
    col=colist, what=c(1,1,1,0))
  dev.off()
}

png(file=paste("PCA2/Scores/score1 (scaled) all clusters.png", sep=''), 
  pointsize=16, width=5*512, height=3.5*512)
par(mfrow=c(6, 1))
par(mar=c(2.5, 4, 1.5, 2))
for (p in 1:reaper1a@nGroups) {
  beanplot(score1.std.mat[,p] ~ mergedat2$type, xlab="Disease", ylab="Score",
    col=colist, what=c(1,1,1,0))
}
dev.off()


### weighted "score" to get sensible idea on expression differences 
### across different tumors 

for (i in 1:reaper1a@nGroups) {
  cat("Cluster", i,":\n")
  print(sort(rownames(reaper1a@fit$P)[fit1==i]))
}

# score with unequal weights for genes in each cluster

check3.vec <- rep(0, reaper1a@nGroups)
check3.nosig.list <- list()
score3.mat <- matrix(0, nrow(mergedat), reaper1a@nGroups)
score3.std.mat <- matrix(0, nrow(mergedat), reaper1a@nGroups)
score3.abs.mat <- matrix(0, nrow(mergedat), reaper1a@nGroups)
score3.std.abs.mat <- matrix(0, nrow(mergedat), reaper1a@nGroups)
comp3.list <- list()
num.groups.vec <- rep(0, reaper1a@nGroups)
for (i in 1:reaper1a@nGroups) {
  clustinm <- sort(rownames(reaper1a@fit$P)[fit1 == i])
  rnadatai <- mergedat[, clustinm]
  threshi <- Thresher(rnadatai, method="auer.gervini", scale=TRUE, agfun=agDimTwiceMean)
  check3.vec[i] <- threshi@pcdim
  check3.nosig.list[[i]] <- colnames(rnadatai)[threshi@delta<=0.3]
  reaperi <- Reaper(threshi, useLoadings=TRUE, method="auer.gervini")
  num.groups.vec[i] <- reaperi@nGroups
  compimat <- reaperi@spca@components[, 1]
  comp3.list[[i]] <- compimat
  comp3.good.gene <- setdiff(clustinm, check3.nosig.list[[i]])
  score3.mat[, i] <- reaperi@data[, comp3.good.gene] %*% compimat  # unscaled data (unsigned weights, could be negative)
  score3.std.mat[, i] <- scale(reaperi@data[, comp3.good.gene]) %*% compimat  # scaled data
  score3.abs.mat[, i] <- reaperi@data[, comp3.good.gene] %*% abs(compimat) # unscaled data with positive weights
  score3.std.abs.mat[, i] <- scale(reaperi@data[, comp3.good.gene]) %*% abs(compimat) # scaled data
  png(file=paste("PCA2/Scores2/cluster", i, " pcdim.png", sep=''), pointsize=16, 
    width=3*512, height=1.5*512)
  plot(AuerGervini(threshi@spca))
  abline(h=1, lty=2, col=2)
  dev.off()
}

for (i in 1:reaper1a@nGroups) {
  png(file=paste("PCA2/Scores2/score3 (no scale) cluster", i, ".png", sep=''), 
    pointsize=18, width=5*512, height=1.5*512)
  beanplot(-score3.mat[, i] ~ mergedat2$type, xlab="Disease", ylab=paste("Weighted PC ",
    1, " Score", sep=''), col=colist, what=c(1,1,1,0))
  dev.off()
}

for (i in 1:reaper1a@nGroups) {
  png(file=paste("PCA2/Scores2/score3 (scaled) cluster", i, ".png", sep=''), 
    pointsize=18, width=5*512, height=1.5*512)
  beanplot(-score3.std.mat[, i] ~ mergedat2$type, xlab="Disease", ylab=paste("Weighted PC ",
    1, " Score", sep=''), col=colist, what=c(1,1,1,0))
  dev.off()
}

for (i in 1:reaper1a@nGroups) {
  png(file=paste("PCA2/Scores2/score3 (no scale, abs) cluster", i, ".png", sep=''), 
    pointsize=18, width=5*512, height=1.5*512)
  beanplot(score3.abs.mat[, i] ~ mergedat2$type, xlab="Disease", ylab=paste("Weighted PC ",
    1, " Score", sep=''), col=colist, what=c(1,1,1,0))
  dev.off()
}

for (i in 1:reaper1a@nGroups) {
  png(file=paste("PCA2/Scores2/score3 (scaled, abs) cluster", i, ".png", sep=''), 
    pointsize=18, width=5*512, height=1.5*512)
  beanplot(score3.std.abs.mat[, i] ~ mergedat2$type, xlab="Disease", ylab=paste("Weighted PC ",
    1, " Score", sep=''), col=colist, what=c(1,1,1,0))
  dev.off()
}

save.image("merge2.Rda")


## Use derived scores to dichotomize and re-define binary vectors

dich.score1.mat <- score1.mat
for (i in 1:ncol(score1.mat)) {
  indi <- which(dich.score1.mat[, i] >= median(dich.score1.mat[, i]))
  dich.score1.mat[indi, i] <- 1
  dich.score1.mat[-indi, i] <- 0
}

head(dich.score1.mat)

for (k in 1:length(tumorlist2)) {
  sdir <- paste("Dichotomization2/", tumorlist2[k], sep='')
  if (!file.exists(sdir)) dir.create(sdir)
}

for (k in 1:length(tumorlist2)) {
  png(file=paste("Dichotomization2/", tumorlist2[k], "/", tumorlist2[k], " Score 1 (9 clusters).png", sep=''), 
    pointsize=18, width=3*512, height=2*512)
  par(mfrow=c(2, 3))
  par(mar=c(2.5, 4, 1.5, 2))
  for (l in 1:ncol(score1.mat)) {
    indk <- which(mergedat2$type==tumorlist2[k])
    beanplot(score1.mat[indk, l] ~ mergedat2$type[indk], xlab="Disease", ylab=
      paste("Score 1: cluster", l, sep=' '), col=colist[[k]], what=c(0,1,0,0))
    abline(h=median(score1.mat[, l]), lty=2, col=2)
  }
  dev.off()
}

for (k in 1:length(tumorlist2)) {
  png(file=paste("Dichotomization2/", tumorlist2[k], "/", tumorlist2[k], " Score 1 (2D).png", sep=''), 
    pointsize=18, width=4*512, height=2*512)
  par(mfrow=c(3, 5))
  par(mar=c(4, 4, 1.2, 2))
  for (l1 in 1:(ncol(score1.mat)-1)) {
    for (l2 in (l1+1):ncol(score1.mat)) {
      indk <- which(mergedat2$type==tumorlist2[k])
      plot(score1.mat[indk, l1], score1.mat[indk, l2], xlab=paste("S1: cluster", l1, sep=' '), 
        ylab=paste("S1: cluster", l2, sep=' '))
      abline(v=median(score1.mat[, l1]), lty=2, col=2)
      abline(h=median(score1.mat[, l2]), lty=2, col=2)
    }
  }
  dev.off()
}

for (k in 1:length(tumorlist2)) {
  png(file=paste("Dichotomization2/", tumorlist2[k], "/", tumorlist2[k], " Score 2 (9 clusters).png", sep=''), 
    pointsize=18, width=3*512, height=2*512)
  par(mfrow=c(2, 3))
  par(mar=c(2.5, 4, 1.5, 2))
  for (l in 1:ncol(score3.mat)) {
    indk <- which(mergedat2$type==tumorlist2[k])
    beanplot(-score3.mat[indk, l] ~ mergedat2$type[indk], xlab="Disease", ylab=
      paste("Score 2: cluster", l, sep=' '), col=colist[[k]], what=c(0,1,0,0))
    abline(h=-median(score3.mat[, l]), lty=2, col=2)
  }
  dev.off()
}

for (k in 1:length(tumorlist2)) {
  png(file=paste("Dichotomization2/", tumorlist2[k], "/", tumorlist2[k], " Score 2 (2D).png", sep=''), 
    pointsize=18, width=3*512, height=1.5*512)
  par(mfrow=c(3, 5))
  par(mar=c(4, 4, 1.2, 2))
  for (l1 in 1:(ncol(score3.mat)-1)) {
    for (l2 in (l1+1):ncol(score3.mat)) {
      indk <- which(mergedat2$type==tumorlist2[k])
      plot(-score3.mat[indk, l1], -score3.mat[indk, l2], xlab=paste("S2: cluster", l1, sep=' '), 
        ylab=paste("S2: cluster", l2, sep=' '))
      abline(v=-median(score3.mat[, l1]), lty=2, col=2)
      abline(h=-median(score3.mat[, l2]), lty=2, col=2)
    }
  }
  dev.off()
}


##############################################################################
# bean plots with sorted mean
library(Matrix)

for (p in 1:reaper1a@nGroups) {
  png(file=paste("PCA2v/Scores/score1 (no scale) cluster", p, "v.png", sep=''), 
    pointsize=25, width=4*512, height=2*512)
  xidat <- data.frame(x=score1.mat[, p], y=paste(mergedat2$type))
  ymean <- aggregate(. ~ y, xidat, mean)
  orderi <- order(ymean$x)
  invorderi <- invPerm(orderi)
  colisti <- colist
  newidat <- xidat
  newidat$y <- as.numeric(newidat$y)
  for (s in 1:length(colv)) {
    colisti[[s]] <- colv[orderi[s]]
    indi <- which(xidat$y==tumorlist2[s])
    newidat[indi, "y"] <- invorderi[s]
  } 
  newidat$y <- as.factor(newidat$y)
  levels(newidat$y) <- tumorlist2[orderi]
  beanplot(x ~ y, data=newidat, xlab="Disease", ylab="Score", 
    col=colisti, what=c(1,1,1,0))
  dev.off()
}

for (p in 1:reaper1a@nGroups) {
  png(file=paste("PCA2v/Scores/score1 (scaled) cluster", p, ".png", sep=''), 
    pointsize=16, width=5*512, height=2*512)
  xidat <- data.frame(x=score1.std.mat[, p], y=paste(mergedat2$type))
  ymean <- aggregate(. ~ y, xidat, mean)
  orderi <- order(ymean$x)
  invorderi <- invPerm(orderi)
  colisti <- colist
  newidat <- xidat
  newidat$y <- as.numeric(newidat$y)
  for (s in 1:length(colv)) {
    colisti[[s]] <- colv[orderi[s]]
    indi <- which(xidat$y==tumorlist2[s])
    newidat[indi, "y"] <- invorderi[s]
  } 
  newidat$y <- as.factor(newidat$y)
  levels(newidat$y) <- tumorlist2[orderi]
  beanplot(x ~ y, data=newidat, xlab="Disease", ylab="Score", 
    col=colisti, what=c(1,1,1,0))
  dev.off()
}

for (i in 1:reaper1a@nGroups) {
  png(file=paste("PCA2v/Scores2/score3 (no scale) cluster", i, ".png", sep=''), 
    pointsize=18, width=5*512, height=1.5*512)
  xidat <- data.frame(x=-score3.mat[, i], y=paste(mergedat2$type))
  ymean <- aggregate(. ~ y, xidat, mean)
  orderi <- order(ymean$x)
  invorderi <- invPerm(orderi)
  colisti <- colist
  newidat <- xidat
  newidat$y <- as.numeric(newidat$y)
  for (s in 1:length(colv)) {
    colisti[[s]] <- colv[orderi[s]]
    indi <- which(xidat$y==tumorlist2[s])
    newidat[indi, "y"] <- invorderi[s]
  } 
  newidat$y <- as.factor(newidat$y)
  levels(newidat$y) <- tumorlist2[orderi]
  beanplot(x ~ y, data=newidat, xlab="Disease", ylab=paste("Weighted PC ", 1, 
    " Score", sep=''), col=colisti, what=c(1,1,1,0))
  dev.off()
}

for (i in 1:reaper1a@nGroups) {
  png(file=paste("PCA2v/Scores2/score3 (scaled) cluster", i, ".png", sep=''), 
    pointsize=18, width=5*512, height=1.5*512)
  xidat <- data.frame(x=-score3.std.mat[, i], y=paste(mergedat2$type))
  ymean <- aggregate(. ~ y, xidat, mean)
  orderi <- order(ymean$x)
  invorderi <- invPerm(orderi)
  colisti <- colist
  newidat <- xidat
  newidat$y <- as.numeric(newidat$y)
  for (s in 1:length(colv)) {
    colisti[[s]] <- colv[orderi[s]]
    indi <- which(xidat$y==tumorlist2[s])
    newidat[indi, "y"] <- invorderi[s]
  } 
  newidat$y <- as.factor(newidat$y)
  levels(newidat$y) <- tumorlist2[orderi]
  beanplot(x ~ y, data=newidat, xlab="Disease", ylab=paste("Weighted PC ", 1, 
    " Score", sep=''), col=colisti, what=c(1,1,1,0))
  dev.off()
}


#################################################################################
## permutation test to see whether removed tumor samples are affecting structures 
## of clusters or not

tumorlist
tumorda

rawmergedat <- NULL
for (i in 1:length(tumorda)) {
  load(tumorda[[i]])
  rawmergedat <- rbind(rawmergedat, rnadata1t)
}

nrow(rawmergedat) - nrow(mergedat)
sampnum <- nrow(mergedat)

M <- 500
pcdmat <- matrix(0, M, 8)
clustvec <- rep(0, M)

ptm <- proc.time()
for (m in 1:M) {
  samplem <- sort(sample(nrow(rawmergedat), sampnum, replace=FALSE))
  mergedatm <- rawmergedat[samplem, ]
  spcam <- SamplePCA(t(scale(mergedatm)))
  objm <- AuerGervini(spcam)
  pcdmat[m, ] <- compareAgDimMethods(objm, agfuns) 
  threshma <- Thresher(mergedatm, method="auer.gervini", scale=TRUE, agfun=f)
  colnames(mergedatm)[threshma@delta<=0.3] 
  reaperma <- Reaper(threshma, useLoadings=TRUE, method="auer.gervini")
  clustvec[m] <- reaperma@nGroups 
}
proc.time() - ptm

ag <- c("TwiceMean", "Spectral", "Kmeans", "Kmeans3", "Ttest", "Ttest2",
  "CPT", "CPM") 

par(mfrow=c(4, 2))
par(mar=c(2,2,2,1))
for (n in 1:8) {
  hist(pcdmat[, n], breaks=50, xlim=c(0, 55), main=paste("Histogram of Component Number: ",
    ag[n], sep=''))
}

X11()

hist(clustvec, breaks=50, xlim=c(0,20))


## Plot the mean scores of each cluster within each tumor type

mean.score.mat1 <- matrix(0, length(tumorlist2), reaper1a@nGroups)
mean.score.mat1 <- sapply(1:reaper1a@nGroups, function(p) {
  xidat <- data.frame(x=score1.mat[, p], y=paste(mergedat2$type))
  ymean <- aggregate(. ~ y, xidat, mean)
  return(ymean$x)})

colnames(mean.score.mat1) <- paste("Cluster", 1:reaper1a@nGroups, sep=' ')
rownames(mean.score.mat1) <- tumorlist2

mean.score.mat3 <- matrix(0, length(tumorlist2), reaper1a@nGroups)
mean.score.mat3 <- sapply(1:reaper1a@nGroups, function(p) {
  xidat <- data.frame(x=-score3.mat[, p], y=paste(mergedat2$type))
  ymean <- aggregate(. ~ y, xidat, mean)
  return(ymean$x)})

colnames(mean.score.mat3) <- paste("Cluster", 1:reaper1a@nGroups, sep=' ')
rownames(mean.score.mat3) <- tumorlist2

heatmap(mean.score.mat1, scale='none', col=redgreen(64))
X11()
heatmap(mean.score.mat3, scale='none', col=redgreen(64))

heatmap(mean.score.mat1, scale='none', col=blueyellow(64))
X11()
heatmap(mean.score.mat3, scale='none', col=blueyellow(64))

# png(file="heatmap_score1_redgreen.png", pointsize=36, width=3*512, height=4*512)
aspectHeatmap(mean.score.mat1, scale='none', col=redgreen(64), hExp=1.25)
# dev.off()
X11()
aspectHeatmap(mean.score.mat3, scale='none', col=redgreen(64), hExp=1.25)

aspectHeatmap(mean.score.mat1, scale='none', col=redgreen(64), hExp=1.25,
        Rowv=as.dendrogram(hclust(distanceMatrix(t(mean.score.mat1), "euclidean"), 
        "ward.D2")),
        Colv=as.dendrogram(hclust(distanceMatrix(mean.score.mat1, "pearson"),
        "ward.D2")))
X11()
aspectHeatmap(mean.score.mat3, scale='none', col=redgreen(64), hExp=1.25,
        Rowv=as.dendrogram(hclust(distanceMatrix(t(mean.score.mat3), "euclidean"), 
        "ward.D2")),
        Colv=as.dendrogram(hclust(distanceMatrix(mean.score.mat3, "pearson"),
        "ward.D2")))


## Making table which compares two clusterings

clust0 <- read.table("Results/Merge_IL4_clusters(CPM).csv", sep=",", header=TRUE)
clust0 <- clust0[, -1]
table(clust0$cluster, clust1$cluster)

source("label_match.R")
lm <- revlabelMatcher(clust0$cluster, clust1$cluster)
table(clust0$cluster, clust1$cluster)[lm$ii, c(lm$jj, setdiff(1:6, lm$jj))]

write.table(table(clust0$cluster, clust1$cluster)[lm$ii, c(lm$jj, setdiff(1:6, lm$jj))],
    "Summary\\compare_clusters_table.csv", sep=",", col.names=NA, row.names=TRUE)


## Look at paired clusters and double check they indeed have two PCs

check4.mat <- matrix(0, reaper1a@nGroups, reaper1a@nGroups)
check4.nosig.list <- list()
for (i in 1:(reaper1a@nGroups-1)) {
  clustinm <- sort(rownames(reaper1a@fit$P)[fit1 == i])
  check4.nosig.list[[i]] <- list()
  for (j in (i+1):reaper1a@nGroups) {
    clustjnm <- sort(rownames(reaper1a@fit$P)[fit1 == j])
    rnadatai <- mergedat[, c(clustinm, clustjnm)]
    threshi <- Thresher(rnadatai, method="auer.gervini", scale=TRUE, agfun=agDimTwiceMean)
    check4.mat[i, j] <- threshi@pcdim
    check4.nosig.list[[i]][[j]] <- colnames(rnadatai)[threshi@delta<=0.3]
    png(file=paste("Summary/Check Paired Clusters/cluster_", i, "&", j, "_pcdim.png", sep=''), pointsize=16, 
      width=3*512, height=1.5*512)
    plot(AuerGervini(threshi@spca))
    abline(h=threshi@pcdim, lty=3, col=2)
    dev.off()
  }
}

write.table(check4.mat, "Summary\\paired_clusters_pc_dimension_check.csv", sep=",",
     col.names=NA, row.names=TRUE)


## heatmap and further analysis of the data

# heatmap
library(RColorBrewer)

heatmap(scale(score1.mat), scale='none', col=redgreen(64))
hist(scale(score1.mat), breaks=100)

scaled.1 <- t(scale(score1.mat))
hist(scaled.1, breaks = 100)
K <- 2.5
truncated.1 <- scaled.1
truncated.1[scaled.1 > K] <- K
truncated.1[scaled.1 < -K] <- -K

colset <- brewer.pal(19, "Set1")[-6]
# ghc.1 <- hclust(gdm <- distanceMatrix(scale(score1.mat), "pearson"), "ward.D2") # clusters
fhc.1 <- hclust(fdm <- distanceMatrix(t(scale(score1.mat)), "euclidean"), "ward.D2") # tumor samples
#tree.col.1 <- cutree(fhc.1, k=18)
colvec.col.1 <- colcol

heatmap(truncated.1, col=redgreen(64), scale="none", Colv=NA, labCol=NA, 
   ColSideColors=colvec.col.1)

# analysis 

score1.dat <- data.frame(score1.mat)
colnames(score1.dat) <- paste("Cluster", 1:ncol(score1.dat), sep='')
score1.dat$type <- rep(NA, nrow(score1.dat))

for (i in 1:length(ptnum2)) {
  score1.dat$type[(tcumsum2[i]+1):tcumsum2[i+1]] <- tumorlist2[i]
}
score1.dat$type <- as.factor(score1.dat$type)

set.seed(131)
fit.1.rf <- randomForest(type ~ ., data=score1.dat, importance=TRUE, na.action=na.omit,
        do.trace=100)
fit.1.rf

set.seed(123456)
control1 <- trainControl(method="repeatedcv", repeats=3)
fit.1.caret <- train(type ~ ., data=score1.dat, method="pls", tuneLength=30, 
         preProcess=c("center","scale"), trControl=control1)

fit.1.caret.imp <- varImp(fit.1.caret, scale=FALSE)
plot(fit.1.caret.imp)

fit.1.caret.imp$importance
fit.1.caret.imp2 <- fit.1.caret.imp$importance
fit.1.caret.imp2[fit.1.caret.imp$importance < 0.01] <- 0
fit.1.caret.imp2



save.image("merge2.Rda")





