

### Perform Tukey's test to detect significant score differences between tumors
### within each block/cluster.

## load data

# load("merge2.Rda")
# save(score1.mat, mergedat2, file = "score.mat.Rda")
load("score.mat.Rda")
dat <- data.frame(cbind(score1.mat, paste(mergedat2$type)))
colnames(dat) <- c(paste("C", 1:6, sep = ''), "Type")
for (i in 1:6) {
    dat[, i] <- as.numeric(dat[, i])
}
dat$Type <- as.factor(dat$Type)


## use anova and HSD to detect significant different tumor types 

fit <- alist()
output <- alist()

for (i in 1:6) {
    fiti <- aov(dat[, i] ~ dat$Type)
    tukeyi <- TukeyHSD(x = fiti, 'dat$Type', conf.level = 0.95)
    fit[[i]] <- fiti
    output[[i]] <- tukeyi
}

k <- 1
plot(fit[[k]])

for (i in 1:6) {
    filenmi <- paste("PCA2v/ToZachMaterial2/", "TukeyHSD", ".cluster", i, ".csv", 
      sep = '')
    write.table(output[[i]][[1]], filenmi, sep=",", col.names = NA, row.names = TRUE)
}

save.image("score.mat.Rda")








