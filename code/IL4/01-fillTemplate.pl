#! perl -w

use strict;
use warnings;

my $outdir = shift || "Filled";

# figure out today's date in nice ISO sortable format
use Time::localtime;
my $tm = localtime;
my $y = 1900 + $tm->year;
my $m = 1 + $tm->mon;
$m = "0$m" if ($m < 10);
my $d = $tm->mday;
$d = "0$d" if ($d < 10);

# slurp the unfilled template source file
my $rmdfile = shift || "01-study33cancers.Rmd";
my $unfilled = undef;
{
    local $/ = undef; # slurpable
    open(RMD, "<$rmdfile") or die "Unable to open '$rmdfile': $!\n";
    $unfilled = <RMD>;
    close(RMD);
}

# specify the range of values we want to use
my @ntype = (18, 33);
my @scale = ("log", "linear");
my @dataset = ("all", "tumor", "normal");
my @action = ("agDimTwiceMean", "agDimCPT");
my @space = ("PC", "original");

# ridiculously deeply nested loops
my %itemkey = (DATE => "$y-$m-$d");
foreach my $nt (@ntype) {
    $itemkey{NTYPE} = $nt;
    foreach my $sc (@scale) {
	$itemkey{SCALE} = $sc;
	foreach my $ds (@dataset) {
	    $itemkey{DATASET} = $ds;
	    foreach my $act (@action) {
		$itemkey{ACTION} = $act;
		foreach my $sp (@space) {
		    $itemkey{SPACE} = $sp;
		    my $filled = $unfilled;
		    foreach my $k (keys(%itemkey)) {
			$filled =~ s/$k/$itemkey{$k}/g;
		    }
		    my $out = "$outdir/$itemkey{NTYPE}-$itemkey{SCALE}-$itemkey{DATASET}".
			"-$itemkey{ACTION}-$itemkey{SPACE}.Rmd";
		    print STDERR "Making $out\n";
		    open(OUT, ">$out") or die "Unable to create '$out': $!\n";
		    print OUT $filled;
		    close OUT;
		}
	    }
	}
    }
}

exit;
__END__
