################################################################
### Idea is to use TCGA data to find possibel housekeeping genes

#####################
### read the data set
source("00-paths.R")
load(file.path(paths$clean, "allData.Rda"))

### define a short-hand banplot function
library(beanplot)
bp <- function(W, ...) {
  beanplot(log2(1+allData[W,]) ~ cancerType,
           main=rownames(anno)[W],
           what=c(1,1,1,0),
           col=listcol, ...)
}

### Pick some cool colors for 33 kinds of cancer
library(Polychrome)
set.seed(9641)
Dark33 <- createPalette(33, c("#2A95E8", "#E5629C"), range = c(10, 60),
   M = 100000)
names(Dark33) <- colorNames(Dark33)
clucol <- Dark33[as.numeric(cancerType)]
listcol <- as.list(Dark33)

### Look at the three examples given in a draft manuscript
### that I rejected....
wh <- which(rownames(anno) == "HNRNPL")
plot(allData[wh,], col=clucol)
bp(wh)

wp <- which(rownames(anno) == "PCBP1")
plot(allData[wp,], col=clucol)
bp(wp)

wr <- which(rownames(anno) == "RER1")
plot(allData[wr,], col=clucol)
bp(wr)

opar <- par(mfrow=c(3,1))
bp(wh)
bp(wp)
bp(wr)
par(opar)

### Also look at those old standbys, GAPDH and beta actin.
wg <- which(rownames(anno) == "GAPDH")
plot(allData[wg,], col=clucol)
bp(wg)

wa <- which(rownames(anno) == "ACTB")
plot(allData[wa,], col=clucol)
bp(wa)

#####################
### Now compute some (possibly) useful) statistics
library(oompaBase)

target <- file.path(paths$scratch, "allCancerStats.rda")
if (file.exists(target)) {
  load(target)
} else {
  ## per-cancer type metrics

  ## mean
  mmm <- sapply(names(ptnum), function(ctype) {
    x <- allData[, cancerType == ctype]
    as.vector(matrixMean(x))
  })
  ## standard deviation
  sss <- sapply(names(ptnum), function(ctype) {
    x <- allData[, cancerType == ctype]
    as.vector(sqrt(matrixVar(x, mmm[,ctype])))
  })
  ## coefficient of variation
  CV <- sss/mmm
  ## limited fold change
  q05 <- sapply(names(ptnum), function(ctype) {
    x <- allData[, cancerType == ctype]
    apply(x, 1, quantile, 0.05)
  })
  q95 <- sapply(names(ptnum), function(ctype) {
    x <- allData[, cancerType == ctype]
    apply(x, 1, quantile, 0.95)
  })
  FC <- q95/q05
  rm(q05, q95)
  ## full spread fold change
  spread <- sapply(names(ptnum), function(ctype) {
    x <- allData[, cancerType == ctype]
    apply(x, 1, function(z) {
      r <- range(z) + 1
      r[2]/r[1]
    })
  })
  rownames(spread) <- rownames(FC) <- rownames(CV) <-
    rownames(sss) <- rownames(mmm) <- rownames(allData)

  ## global analyses

  numzero <- apply(allData, 1, function(x) sum(x == 0))
  mm <- as.vector(matrixMean(allData))
  ss <- as.vector(sqrt(matrixVar(allData, mm)))
  cvall <- ss/mm
  fcall <- apply(allData, 1, function(z) {
    r <- quantile(z, c(0.05, 0.95)) + 1
    r[2]/r[1]
  })
  spreadall <- apply(allData, 1, function(z) {
    r <- range(z) + 1
    r[2]/r[1]
  })
  names(spreadall) <- names(fcall) <- names(cvall) <-
    names(mm) <- names(ss) <- rownames(allData)

  save(mmm, sss, CV, FC, spread, numzero,
       mm, ss, cvall, fcall, spreadall,
       file = target)
}
rm(target)

#################################
minFC <- apply(FC, 1, min)
maxFC <- apply(FC, 1, max)
summary(maxFC[numzero < 10])

table(nz <- trunc(log2(1+numzero)))

filt <- numzero < 10
plot(log2(minFC[filt]), log2(maxFC[filt]), ylim=c(0,10), xlim=c(0,10))
abline(0,1)
abline(h=1, v=1)


sum(candidate <- minFC < 2 & maxFC < 3 & numzero < 10)
summary(minSpread[candidate])
summary(maxSpread[candidate])

foo <- allData[candidate,]
coco <- cor(t(foo))

hc <- hclust(as.dist(1-coco), "ward.D2")
hcd <- as.dendrogram(hc)
heatmap(coco, Rowv=hcd, Colv=hcd, col=cyanyellow(64), scale='none', zlim=c(-1,1))


  cv <- t(CV[gl <- c(wh, wp, wr, wg, wa), ])
  summary(cv)
summary(t(sss[gl,]))
summary(t(mmm[gl,]))

moo <- t(mmm[gl,])
moo[order(moo[,1]),]

soo <- t(FC[gl,])
soo[order(soo[,1]),]

foo <- t(FC[gl,])
foo[order(foo[,1]),]

cv[order(cv[,1]),]


filt <- mm > 100 & numzero < 10
summary(filt)
summary(mm[filt])
summary(ss[filt])
summary(cvall[filt])
w <- which(filt & cvall < 0.014)
plot(x <- allData[w,], col=clucol, pch=16)
bp(w)
plot(x[cancerType == "HNSC"], ylim=c(0, 100))
plot(x, ylim=c(0, 100), pch=16, col=clucol)




  
image(1:33, 1:5, cv, col=jetColors(64), xaxt='n', yaxt='n', xlab='', ylab='', main="CV")
mtext(colnames(cv), side=2, at=1:5, line=1)
mtext(rownames(cv), side=1, at=1:33, line=1, las=3)


yl <- c(9,19)
opar <- par(mfrow=c(3,1))
bp(wa, ylim=yl)
bp(wr, ylim=yl)
bp(wh, ylim=yl)
par(opar)

meanCV <- as.vector(matrixMean(CV))
maxCV <- apply(CV, 1, max)
w <- which(meanCV < 0.18)
cvall[w]
bp(w)

fc <- apply(allData, 1, function(x) {
  q <- quantile(x, c(0.05, 0.95), na.rm=TRUE)
  q[2]/q[1]
})
summary(fc[filt])

smoothScatter(log(fc[filt]), cvall[filt])

pairs(data.frame(log(fc), cvall, log(1+mm))[filt,])
