---
title: "Using Thresher to Study Transcription Factors"
author: "Kevin R. Coombes"
date: "`r Sys.Date()`"
output:
  html_document:
    highlight: kate
    theme: yeti
    toc: true
---

```{r mycss, results="asis", echo=FALSE}
cat('
<style type="text/css">
b, strong {color: red; }
i, em {color: blue; }
.defn {color: purple; }
.para {color: purple;
      font-weight: bold;
}
.figure { text-align: center; }
.caption { font-weight: bold; }
</style>
')
```

# Compute Distances
Here we want to compute the distance of each patient sample from the center
of its known cancer type. However, we actually go ahead and compute the
distances from each patient to **all** of the different cancer type centers.
```{r compdis}
euc <- matrix(NA, nrow=nrow(thisData), ncol=nrow(cancerMean))
for (k in 1:nrow(cancerMean)) {
  temp <- sqrt(apply(sweep(unweightedScore, 2, cancerCenters[k,], '-')^2, 1, sum))
  euc[,k] <- temp
}
colnames(euc) <- as.character(cancerMean[,1])
dim(euc)
summary(euc)
```

Next, we'd like to compute the (analopg of?) silhouette widtrhs. These were
defined by Kaufman and Rouseeuw as
$$ \frac{b_i - a_i}{\textrm{max}(a_i, b_i)} $$
wher $a_i$ is the averager distance from the ith sample to its assigned cluster
and $b_i$ is its distance to the closest other cluster.
```{r}
f <- 'sil.Rda'
if (file.exists(f)) {
  load(f)
} else {
  intCancerType <- as.numeric(thisAnno$cancerType)
  dm <- dist(thisData)
  realSil <- silhouette(intCancerType, dist=dm)
  save(intCancerType, dm, realSil, file=f)
}
rm(f)
```

```{r}
# compute dista
ai <- sapply(1:nrow(euc), function(i) {
  euc[i, intCancerType]
})

closest <- apply(euc, 1, which.min) # index

ai <- sapply(1:nrow(euc), function(i) {
  euc[i, as.numeric(thisAnno$cancerType[i])]
})
b0i <- sapply(1:nrow(euc), function(i) {
  euc[i, closest[i]]
})
crossed <- table(closest, thisAnno$cancerType)

segundo <- apply(euc, 1, function(x) {
  two <- sort(x)[2]
  which(x==two)
})
b1i <- sapply(1:nrow(euc), function(i) {
  euc[i, segundo[i]]
})
bi <- ifelse(b0i == ai, b1i, b0i)
sil <- (bi - ai)/pmax(ai, bi)
summary(sil)
beanplot(sil ~ thisAnno$cancerType, xlab="Disease", ylab="Silhouette", 
    col=colist, what=c(1,1,1,0))

barplot(sil, col=colorScheme[thisAnno$cancerType], space=0,border=NA)

temdf <- data.frame(sil=sil, type=thisAnno$cancerType)
agg <- aggregate(sil ~ type, temdf, mean)
silly <- agg$sil
names(silly) <- as.character(agg$type)
barplot(silly, col=colorScheme, ylim=c(min(silly), 1))

tab <- table(closest, segundo)

cpca <- SamplePCA(t(cancerCenters))
plot(cpca@scores[,1], cpca@scores[,2], type='n', xlab="PC1", ylab="PC2")
text(cpca@scores[,1], cpca@scores[,2], names(silly))

```

# Appendix
This analysis was performed in the following directory.
```{r where}
getwd()
```
This analysis was performed using the following R packages.
```{r si}
sessionInfo()
```
