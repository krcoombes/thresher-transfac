TG <- read.table(file.path("..", "results", "toppgene", "00interpret.tsv"),
                 sep="\t", header=TRUE)
UG <- read.csv(file.path("..", "results", "unigene-maps.csv"))

load(file.path("..", "scratch", "101-intermediate.Rda"))

top5 <- sapply(1:ncol(cancerCenters), function(I) {
  ot <- rev(order(cancerCenters[,I]))
  paste(rownames(cancerCenters)[ot][1:5], collapse = "; ")
})

combo <- data.frame(TG, Top5=top5, UG)[, -5]
write.csv(combo, file = file.path("..", "results", "interpret.csv"),
          row.names=FALSE)
