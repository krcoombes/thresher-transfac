---
title: "Using Thresher to Study Transcription Factors"
author: "Kevin R. Coombes"
date: "`r Sys.Date()`"
output:
  html_document:
    highlight: kate
    theme: yeti
    toc: true
---

```{r opts, echo=FALSE}
knitr:::opts_chunk$set(fig.path="../results/knitfigs/")
```

```{r mycss, results="asis", echo=FALSE}
cat('
<style type="text/css">
b, strong {color: red; }
i, em {color: blue; }
.defn {color: purple; }
.para {color: purple;
      font-weight: bold;
}
.figure { text-align: center; }
.caption { font-weight: bold; }
</style>
')
```

# Getting Started
First, we load some global information about where to find pieces of
this project. (This binary R data file was created by the script
`100prepData.R`, which also collects transcription factors from the
full TCGA RNA-Sequencing data set.)
```{r globals}
source("00-paths.R")
load(file.path(paths$scratch, "globals.Rda"))
ls()
```
Next, we set up a location to store the results.
```{r resultDir}
resultDir <- "../results"
if (!file.exists(resultDir)) dir.create(resultDir)
outputDir <- file.path(resultDir, "ManyFigs")
if (!file.exists(outputDir)) dir.create(outputDir)
```

# Merging Data Across Cancer Types
We load in the data for all transcription factor genes, across different
cancer types from TCGA.  While loading the data, we also
create a vector to keep track of the cancer type of each sample.
```{r allData}
allData <- NULL
ptnum <- rep(NA, length(tumorlist))
names(ptnum) <- tumorlist
cancerType <- NULL
for (tumor in tumorlist) {
  fname <- file.path(rdaDir,
                     paste(tumor, "Rda", sep='.'))
  load(fname)
  pnum <- nrow(rnadata1t)
  # skip small data sets unless we said we want 'all'
  if (33 < 33 & pnum < 200) next 
  allData <- rbind(allData, rnadata1t)
  ptnum[tumor] <- pnum
  cancerType <- c(cancerType, rep(tumor, pnum))
  rm(genename1, rnadata1t)
}
rm(tumor)
```

## Color Scheme
Next, we have to make a set of colors using the `Polychrome` package.
```{r colist}
library(Polychrome)
colorScheme <- palette36.colors(36)[-c(1:2,36)]
names(colorScheme) <- tumorlist
colist <- as.list(colorScheme)
```
Here is a picture of the color scheme.
```{r scheme, fig.cap="Color scheme.", fig.height=6, fig.width=9}
opar <- par(mai=c(0.2, 0.2, 0.8, 0.2))
swatch(colorScheme)
par(opar)
```
We also create and save a different version of this color scheme, which
we can use as a legend for other figures.
```{r}
png(file=file.path(resultDir, "legend.png"), 
                   pointsize=16, width=1.3*512, height=1.2*512)
opar <- par(mai = c(0.3, 0.3, 0.3, 0.3))
plot(0,0, type='n', xlim=c(-1, 0.45), ylim=c(0, 1), axes=FALSE, xlab="", ylab="")
par(cex=2.2)
legend(-1.0, 1, tumorlist[1:11],  col=colorScheme[1:11],  pch=15)
legend(-0.5, 1, tumorlist[12:22], col=colorScheme[12:22], pch=15)
legend( 0.0, 1, tumorlist[23:33], col=colorScheme[23:33], pch=15)
par(opar)
rm(opar)
dev.off()
```

![Legend for cancer types.](../results/legend.png)

## Data Transformation
The next line may look odd because of templatization, but we want to
be able to run this analysis on either the linear-raw scale or on the
log-transformed scale.
```{r which.scale}
if ("log" == "log") {
   allData <- log2(1 + allData)
}
```

Now we count some things.
```{r counts}
ptnum
sum(ptnum)
```

The TCGA barcodes include a part that identifies the sample types.
Types 01--09 are actual tumors; types 10--19 are different kinds of
normals.  We start by extracting this information.
```{r st}
rn <- rownames(allData)
st <- substring(rn, 14,15)
threeway <- rep("Tumor", length(st))
threeway[st == "11"] <- "Normal"
threeway[st %in% c("06", "07")] <- "Metastatic"
threeway <- factor(threeway, levels=c("Tumor", "Metastatic", "Normal"))
allAnno <- data.frame(cancerType=cancerType, sampleType=st, Cat=threeway)
rm(rn, st, threeway)
summary(allAnno)
```
We see that there are `r sum(allAnno$sampleType=="11")` normal samples (from different
kinds of cancer). Now we use this information to separate the normal
samples from the tumor samples.
```{r normalData}
normalData <- allData[allAnno$sampleType == 11,]
normalAnno <- allAnno[allAnno$sampleType == 11,]
```

```{r tumorData}
tumorData <- allData[allAnno$sampleType != 11,]
tumorAnno <- allAnno[allAnno$sampleType != 11,]
```

We may also want to run the same analysis separately on these three
different data sets. Here we figure out which version we actually want
to use right now.
```{r}
thisAnno <- allAnno
thisData <- allData
dim(thisData)
```
# Determining the PC Dimension
We have investigated various methods for determining the "true"
dimension of the space of principal components.

## Variants of Bartlett's Method
Here we use various forms of Bartlett's method. They all clearly give
the wrong answer.
```{r bart}
suppressMessages( library(nFactors) )
system.time(
    nBartlett(data.frame(scale(thisData)), nrow(thisData)) 
)
```

## The Broken-Stick Model
Another standard way to determine the PC dimension is with the
broken-stick model.
```{r setupBS}
suppressMessages( library(PCDimension) )
system.time(
  spca <- SamplePCA(t(scale(thisData)))
)
lam <- spca@variances[1:(ncol(thisData) - 1)] 
bsDimension(lam) 
```
This approach at least seems to give a more plausible answer.
```{r plotBS, fig.cap="Scree plot using all data."}
M <- 60
opar <- par(mai=c(0.5, 1, 0.2, 0.2))
bs <- brokenStick(1:486, 486)
pts <- screeplot(spca, xlim=c(0, 50))
lines(pts[1:M,1], bs[1:M], col="blue", type='b', pch=16, lwd=2)
par(opar)
```

## Automated Auer-Gervini Model
In this section, we apply our automated Auer-Gervini method.  The original
Auer-Gervini method relies on visual inspection of a graph of a step
function arising from a Bayesian model.  Our extension uses various
algorithms to automate the procedure of deciding which steps are
"long". Here are the algorithms we will use:
```{r algs}
agfuns <- list(twice=agDimTwiceMean, specc=agDimSpectral,
               km=agDimKmeans, km3=agDimKmeans3, 
               tt2=agDimTtest2,
               cpt=agDimCPT)
```

And here are the results using all data.
```{r, ag.obj}
system.time(
  obj <- AuerGervini(spca)
)
cad <-compareAgDimMethods(obj, agfuns) 
cad
```
Our simulation studies suggested that CPT and Twice Mean gave the most
reliable results, with Twice Mean behaving better when the number of
objects to be clustered (in this case, transcription factors) is small
compared to the number of measurements used for clustering (in this case,
patient samples). Since the answers produced by the methods in this case
are quite different, we will follow the lead of the simulation results
and take the value provided by Twice Mean. 
```{r ag, fig.cap="Auer-Gervini step function using all data."}
PD <- cad[1]
plot(obj, agfuns, ylim=c(0, 50))
```

# Understanding the Principal Components

We take a look at the first two principal components
```{r fig.cap="First two principal components, colored by sample type."}
opar <- par(mai=c(1, 1, 0.2, 0.2))
plot(spca, split=thisAnno$Cat, col=c('red', 'blue', 'green'))
legend("bottomleft", levels(thisAnno$Cat), col=c('red', 'blue', 'green'), pch=15)
B <- 21
abline(B, -1)
par(opar)
```

There is no obvious difference betwen the normal samples, primary tumors,
or metastatic tumors. However, there is a group to the upper right that
clearly is different from the rest of the samples. In order to see if this
grouping is driven by one particular subtype of cancer, we replot the first
two principal components, this time coloring the samples by cancer type.
```{r pca1_2, fig.cap="First two principal components, colored by cancer type."}
opar <- par(mai=c(1, 1, 0.2, 0.2))
plot(spca, split=thisAnno$cancerType, col=colorScheme)
abline(B, -1)
par(opar)
```

Now it is clear that the separate group is composed primarily of cancers
of one type. Here we try to figure out exactly which one.
```{r shred}
shred <- spca@scores[,1] + spca@scores[,2] - B
table(thisAnno$cancerType, shred>0)
```
The subgroup contains almost all of the low grade glioma (LGG)
cases along with a handful of glioblastoma (GBM) samples. No other
cancer types are represented.

Next, we plot the remainder of the signficant principal components.
```{r moreplots, fig.cap="Scatter plots of more principal components."}
opar <- par(mai=c(1, 1, 0.2, 0.2))
plot(spca, split=thisAnno$cancerType, col=colorScheme, which=3:4)
plot(spca, split=thisAnno$cancerType, col=colorScheme, which=5:6)
plot(spca, split=thisAnno$cancerType, col=colorScheme, which=7:8)
plot(spca, split=thisAnno$cancerType, col=colorScheme, which=9:10)
plot(spca, split=thisAnno$cancerType, col=colorScheme, which=11:12)
plot(spca, split=thisAnno$cancerType, col=colorScheme, which=13:14)
plot(spca, split=thisAnno$cancerType, col=colorScheme, which=15:16)
plot(spca, split=thisAnno$cancerType, col=colorScheme, which=17:18)
plot(spca, split=thisAnno$cancerType, col=colorScheme, which=19:20)
plot(spca, split=thisAnno$cancerType, col=colorScheme, which=21:22)
plot(spca, split=thisAnno$cancerType, col=colorScheme, which=23:24)
plot(spca, split=thisAnno$cancerType, col=colorScheme, which=25:26)
plot(spca, split=thisAnno$cancerType, col=colorScheme, which=27:28)
par(opar)
```
Some of the components clearly distinguish one or two subtypes of cancer
from all others. And most of the components do show differential expression
levels between multiple cancer types. These observations give support to
the idea that the larger number of significant PCs found by the TwiceMean
method is likely to correctly represent some interesting biology.

The standard difficulty, of course, is that is really hard to interpret
principal components (either individually or in combination) in terms of
biology. Here we illustrate some of the ways that this problem manifests
itself in the present context. First, we look at the distribution of the 
weights of genes (i.e., transcription factors) in the principal components.

```{r scree, fig.cap="Histogram showing the weights of transcription factors on the first 30 principal components."}
pcs <- spca@components[, 1:PD]
hist(pcs, breaks=123)
```

Based on the histogrm, we can make a plausible argument for using something
near 0.1 for a cutoff to identify "significant" contributions. Now we do
some more counting.
```{r}
apply(pcs, 2, function(x) sum(abs(x) > 0.1))
xx <- apply(spca@components[, 1:30], 1, function(x) sum(abs(x) > 0.1))
table(xx)
```
Roughly half of the transcription factors never make a significant
contribution to any component, while roughly one-quarter contribute
to more than one component. Neither of these answers/conclusions is really
reasonable, nor are they likely to improve interpretability. We believe
that a better measure of the contribution of a transcription factor is to
use the length of its weight vector in the entire (29-dimensional)
principal component space. We will test that idea later using our
`Thresher` package.

# First Pass at Clustering Transcription Factors
One way to describe our goal is that we want to cluster transcription
factors in a way that converts them into a set of one-dimensional,
biologically interpretable component. Before doing that, we are going
to explore some of the more traditional clustering approaches.

First, we look at a principal components analysis on the transposed data set.
```{r tpca, fig.cap="Principal components plot showing transcription factors."}
tpca <- SamplePCA(thisData)
plot(tpca)
```

This figure suggests that we can separate transcription factors into
different groups. But it doesn't tell us how many. So, we turn instead to
hierarchical clustering.
```{r hck, fig.cap="Hierarchical clustering of transcription factors into `r PD` groups."}
hc <- hclust(distanceMatrix(thisData, "pearson"), "ward.D2")
kut <- cutree(hc, k=PD)
plotColoredClusters(hc, lab=colnames(thisData), col=colorScheme[kut])
```

Again, as usual, we don't really know where to cut the tree to get
a reliable split of transcription factors into groups. In this figure,
we have looked at `r PD` groups since that appears to be the principal
component dimension when we viewed the transcription factors (above)
as the features rather than as the objects to be clustered. However,
a scree plot of the transposed data strongly suggests that it is only
one dimensional:
```{r fig.cap="Scree plot of transposed data."}
screeplot(tpca, xlim=c(0,40))
```
And an Auer-Gervini approach gives similar answers.
```{r fig.cap="Auer-Gervini plot of transposed data."}
ag <- AuerGervini(tpca)
compareAgDimMethods(ag, agfuns) 
plot(ag, agfuns, ylim=c(0, 15))
```

# Thresher
Now we use the Thresher algorithm to remove outliers and cluster the
features. Both to reduce typing and to ensure consistency, we use the
following function.
```{r makeClusters}
library(Thresher)
markClusters <- function(dataset, agfun, cutoff) {
  thresh <- Thresher(dataset, method="auer.gervini", scale=TRUE, agfun=agfun)
  outliers <- colnames(dataset)[thresh@delta <= cutoff] 
  reap <- Reaper(thresh, useLoadings = "PC" == "PC", cutoff = cutoff)
  fit1 <- apply(reap@fit$P, 1, which.max)
  clust1 <- rbind(data.frame(RNA = colnames(dataset)[thresh@delta <= cutoff],
                             cluster = rep(0, sum(thresh@delta <= cutoff))),
                  data.frame(RNA = rownames(reap@fit$P), cluster=as.numeric(fit1)))
  clust1$RNA <- as.character(clust1$RNA)
  clust1 <- clust1[order(clust1[,1]), ]
  list(thresher = thresh, reaper = reap, clusters = clust1,
       outliers = outliers, fit1 = fit1)
}
```

We apply this function to all the data.
```{r alltm}
f <- file.path(resultDir, "thisRes.Rda")
if (file.exists(f)) {
  load(f)
} else {
  tic <- system.time(
    thisResult <- markClusters(thisData, agDimTwiceMean, 0.2)
  )
  save(thisResult, tic, file=f)
}
#tic
rm(f)
```
And we look at some of the results.
```{r}
summary(thisResult$reaper@keep) # no outliers
thisResult$outliers             # really
thisResult$thresher@pcdim       # 29
thisResult$reaper@pcdim         # 29, still
thisResult$reaper@nGroups       # 30, so only one extra
thisResult$reaper@bic           # and this is why
PD <- thisResult$reaper@pcdim
NG <- thisResult$reaper@nGroups
```

We save the cluster assignments to a file with comma-separated-values (CSV).
```{r write}
htf <- read.csv(file.path(dataDir, "HumanTF.csv"))
htf <- htf[!is.na(htf$SYMBOL),]
rownames(htf) <- as.character(htf$Hs.SYMBOL)
htf <- htf[as.character(thisResult$clusters$RNA),]
htf$Cluster <- thisResult$clusters$cluster
htf <- htf[, c(1:3, 14, 4:13)]
write.csv(htf,
          file=file.path(resultDir, "geneClust.csv"),
          row.names=FALSE)
htf <- htf[, c(6, 7, 10, 14, 4)]
htf <- htf[order(htf$Cluster, rownames(htf)), ]
write.csv(htf, file=file.path(resultDir, "tfClust.csv"))
```

# Scores
We consider two summary scores for the biological components.  First, we take
a simple unweighted average of the expression levels of genes (i.e., transcription
factors) in the defining cluster. Second, we use each cluster to perform a
separate principal components analysis, and use the (appropriately signed) first
principal component.

## Unweighted (Simple Average)
Here we compute per-sample scores with equal weights by simply averaging over
all genes in each cluster.
```{r}
unweightedScore <- matrix(0, nrow(thisData), NG)
for (i in 1:NG) {
  clustinm <- sort(rownames(thisResult$reaper@fit$P)[thisResult$fit1 == i])
  unweightedScore[, i] <- apply(thisData[, clustinm], 1, mean) # unscaled data
}
colnames(unweightedScore) <- paste("Cluster", 1:NG, sep='')
```

We now produce bean plots of each of the unweighted scores, using cancer type
to separate the samples.
```{r beans}
library(beanplot)
for (p in 1:NG) {
  png(file=file.path(outputDir, paste("unweightedScore-BC", p, ".png", sep='')), 
    pointsize=16, width=5*512, height=1.5*512)
  beanplot(unweightedScore[, p] ~ thisAnno$cancerType, xlab="Disease", ylab="Score", 
    col=colist, what=c(1,1,1,0))
  dev.off()
}
```

## Weighted Scores (Separate First PC)
We want to confirm that each of our gene clusters produces a
one-dimensional principal component space when considered apart from the
other clusters. To accomplish this task, we will get a separate PCA
model for each gene-cluster. We will produce scree plots, Auer-Gervini
plots, and two-dimensional PC scatter plots for each analysis. At the
same time, we will save the scores of the first PC for all samples,
which will serve as a weighted average of gene expression.


```{r}
cam <- matrix(NA, nrow=NG, ncol=length(agfuns)) # AG PC dimensions
pcScore <- matrix(NA, nrow=nrow(thisData), ncol=NG)
for (k in 1:NG) {
  label <- paste("BC", k)
  submat <- allData[, thisResult$clusters$cluster == k]
  mpca <- SamplePCA(t(scale(submat))) # PCA for cluster k
  xs <- mpca@scores[,1]     # sample scores
  wg <- mpca@components[,1] # gene weights on first PC
  ## figure out the appropriate sign
  if (mean(wg > 0) < 0.5) { # more than half negative
    xs <- (-1)*xs           # so negate scores
    wg <- (-1)*wg           # and components
  }
  pcScore[,k] <- xs
  print(mean(wg > 0)) # show that most (all?) are positive
  ## make scree plot
  png(file=file.path(outputDir, paste("scree-BC", k, ".png", sep='')), 
                     pointsize=16, width=1.5*512, height=1.5*512)
  screeplot(mpca, main=label)
  dev.off()
  ## make PC scatter plot
  png(file=file.path(outputDir, paste("scatter-BC", k, ".png", sep='')), 
                     pointsize=16, width=1.5*512, height=1.5*512)
  plot(mpca, split=thisAnno$cancerType, col=colorScheme, main=label)

  dev.off()
  ## make Auer-Gervini plot
  png(file=file.path(outputDir, paste("ag-BC", k, ".png", sep='')), 
                     pointsize=16, width=1.5*512, height=1.5*512)
  ag <- AuerGervini(mpca)
  plot(ag, agfuns, main=label)
  dev.off()
  cam[k,] <- temp <- compareAgDimMethods(ag, agfuns)
}
dimnames(cam)<- list(paste("BC", 1:NG, sep=''),
                     names(temp))
dimnames(pcScore) <- list(row.names(thisData),
                          paste("BC", 1:NG, sep=''))
```
As expected, all weights have the same sign, which we have engineered to 
be positive.


We now produce bean plots of each of the PC-weighted scores, again using cancer
type to separate the samples.
```{r beansprouts}
for (p in 1:NG) {
  png(file=file.path(outputDir, paste("pcScore=BC", p, ".png", sep='')), 
    pointsize=16, width=5*512, height=1.5*512)
  beanplot(unweightedScore[, p] ~ thisAnno$cancerType, xlab="Disease", ylab="Score", 
    col=colist, what=c(1,1,1,0))
  dev.off()
}
```

# Comparing Components
At this point, we have the purely mathematically defined principal components
along with both weighted and unweighted "biological" components. Now we compute
the correlation between these different versions.
```{r}
dim(pcScore)
dim(unweightedScore)
dim(mathScore <- spca@scores[,1:NG])

compMeanMath <- t(scale(unweightedScore)) %*% scale(mathScore)/(nrow(unweightedScore)-1)
compPCMath <- t(scale(pcScore)) %*% scale(mathScore)/(nrow(unweightedScore)-1)
compMeanPC <- t(scale(unweightedScore)) %*% scale(pcScore)/(nrow(unweightedScore)-1)
```

Next, we plot these correlation matrices.
```{r fig.cap="Correlation between different component scores.", fig.width=15, fig.height=5}
rg <- oompaBase::blueyellow(64)
opar <- par(mfrow=c(1,3))
image(compMeanMath, zlim=c(-1,1), col=rg,
      xlab="Principal Components", ylab="Biological Components")
image(compPCMath, zlim=c(-1,1), col=rg,
      xlab="Principal Components", ylab="WtdBiological Components")
image(compMeanPC, zlim=c(-1,1), col=rg,
      xlab="Wtd Biological Components", ylab="Biological Components")
par(opar)
```
This shows (1) the two "biological component" sets are highly correlated
with each other. But the "math components"" all appear to be weighted
linear combinations of the biological components, with the weights more
heavily focused on the first few math components. This makes sense. We have
given up the mathematical ideal of orthogonality in the hope of finding 
a set of components that can be interpreted biologically.

By the way, it appears that we really don't need to do the extra work to
compute the weighted biological components; the clustering and bean plots
are sufficiently similar that we can just use the unweighted averages.

## Sorted Beanplots
In order to make it easier to interpret the clusters, we now generate a set
of bean plots, with the cancer types sorted by the mean (over samples) of
the  unweighted score (averaged over genes in the cluster). First we get the
group means.
```{r cancerMean}
xdat <- data.frame(unweightedScore, Type = thisAnno$cancerType)
cancerMean <- aggregate(. ~ Type, xdat, mean)
```
Because the `aggregate` function includes the classes as the first column,
we are going to reformat this object as a matrix with sensible row names.
```{r cancerCenter}
cancerCenters <- as.matrix(cancerMean[, -1])
row.names(cancerCenters) <- as.character(cancerMean[,1])
```

Now we can create the sorted bean plots.
```{r sortbeans}
baselev <- levels(thisAnno$cancerType)

for (k in 1:NG) {
  scaler <- 1024
  ot <- order(cancerMean[,k+1])
  tempType <- factor(thisAnno$cancerType, 
                     levels=baselev[ot])
  png(file=file.path(outputDir, paste("sorted-BC", k, ".png", sep='')), 
    pointsize=36, width=5*scaler, height=1.5*scaler)
  beanplot(unweightedScore[,k] ~ tempType, col=colist[ot[]], what=c(1,1,1,0),
           main=paste("BC", k))
  dev.off()
}
```


# Things That Don't Work
```{r compdis}
peuc <- matrix(NA, nrow=nrow(thisData), ncol=nrow(cancerMean))
for (k in 1:nrow(cancerMean)) {
  temp <- sqrt(apply(sweep(pcScore, 2, cancerCenters[k,], '-')^2, 1, sum))
  peuc[,k] <- temp
}
colnames(peuc) <- as.character(cancerMean[,1])
pclosest <- apply(peuc, 1, which.min)

pcrossed <- table(factor(pclosest, levels=1:33), thisAnno$cancerType)
table(pclosest)

```

```{r km}
km <- kmeans(unweightedScore, cancerCenters)
table(km$cluster)
```

# Intermediate Results
We save some of the critical objects for use in later analyses.
```{r save}
save(colorScheme, colist,       # color schemes
     allData, allAnno,          # transcription factor expression in all samples
     unweightedScore, pcScore,  # scores
     cancerMean, cancerCenters, # mean unweighted scores by cancer type
     file=file.path("..", "scratch", "101-intermediate.Rda"))
```

# Appendix
This analysis was performed in the following directory.
```{r where}
getwd()
```
This analysis was performed using the following R packages.
```{r si}
sessionInfo()
```
