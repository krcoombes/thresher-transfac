#! perl -w

use strict;
use warnings;

### The source file contains a table in tab-separated-value format. 
### However, it omits cell contents if they are the same as the row above.
### This script fills in the omitted data.

my $source = "../data/raw/tfcat-list.txt";
my $target = "../data/raw/refill.txt";
open(SRC, "<$source") or die "Unable to read '$source': $!\n";
open(TGT, ">$target") or die "Unable to create '$target': $!\n";
my $line = <SRC>;
print TGT $line; # copy the header
chomp $line;
my @olditems = split /\t/, $line;
print STDERR join("\n", @olditems), "\n";
while($line = <SRC>) {
    chomp $line;
    my @items = split /\t/, $line;;
    foreach my $i (0..$#items) {
	unless (defined($items[$i]) && $items[$i] ne '') {
	    $items[$i] = $olditems[$i];
	}
    }
    print TGT join("\t", @items), "\n";
    @olditems = @items;
}
close(TGT);
close(SRC);

exit;
__END__
