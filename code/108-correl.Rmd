---
title: "Correlation With Transcription Factors"
author: "Kevin R. Coombes"
date: '`r Sys.Date()`'
output:
  html_document:
    highlight: kate
    theme: yeti
    toc: yes
---

```{r globopt}
knitr:::opts_chunk$set(fig.path="../results/knit8figs/")
```

```{r mycss, results="asis", echo=FALSE}
cat('
<style type="text/css">
b, strong {color: red; }
i, em {color: blue; }
.defn {color: purple; }
.para {color: purple;
      font-weight: bold;
}
.figure { text-align: center; }
.caption { font-weight: bold; }
</style>
')
```

# Getting Started
First, we load some global information about where to find pieces of
this project. (This binary R data file was created by the script
`100prepData.R`, which also collects transcription factors from the
full TCGA RNA-Sequencing data set.)
```{r globals}
source("00-paths.R")
scratch <- paths$scratch
load(file.path(scratch, "globals.Rda"))
rm(allCancers, rawDataDir, rdaDir)
ls()
```
We also load the intermediate results from the first analysis,
performed in `101-studyTF.Rmd`.
```{r inter}
load(file.path(scratch, "101-intermediate.Rda"))
rm(allData, cancerMean, pcScore)
ls()
```

We also load a list of the transcription factors, along with the clusters to which they belong.
```{r tfs}
bcs <- read.csv(file.path("..", "results", "geneClust.csv"))
dim(bcs)
```

# Megadata
Here we load the full RNA-Sequencing data set for all 33 TCGA cancer
types; this binary R data file was created by report `103-OddsNEvens`.
```{r mega}
load(file.path(scratch, "megadata.Rda"))
ls()
```

# SuperCorrelation

```{r super}
f <- file.path(scratch, "cory.Rda")
if (file.exists(f)) {
  load(f)
} else {
  scaledScore <- scale(unweightedScore)
  rm(uweightedScore)
  gc()
  scaledExpression <- t(scale(t(megadata)))
  rm(megadata)
  gc()
  cory <- scaledExpression %*% scaledScore / (ncol(scaledExpression) - 1)
  rm(scaledScore, scaledExpression)
  gc()
  foo <- which(apply(cory, 1, function(x) all(is.na(x))))
  cory <- cory[-foo,]
  save(cory, file=f)
}
rm(f)
dim(cory)
```


```{r}
library(oompaBase)
image(cory, zlim=c(-1,1), col=blueyellow(64))
mx <- apply(abs(cory), 1, max)
w <- apply(abs(cory),1, which.max)
table(w)

table(w[mx > 0.5])

#image(cory > 0.6)

ct <- apply(cory, 1, function(x) sum(abs(x) > 0.5))

isTF <- rownames(cory) %in% bcs$Hs.SYMBOL
table(ct[isTF])
table(ct)

summary(mx[isTF])
hist(mx[isTF], breaks=37)

plot(sort(mx[isTF]))

library(beanplot)
beanplot(mx[as.character(bcs$Hs.SYMBOL)] ~ bcs$Cluster)

beanplot(mx ~ w)

apply(cory, 2, function(x) sum(abs(x) > 0.5))

resultsDir <- file.path("..", "results")
if (!file.exists(dp <- file.path(resultsDir, Sys.Date()))) {
  dir.create(dp)
} 
if (!file.exists(dp <- file.path(dp, "genelists"))) {
  dir.create(dp)
} 
for (CO in colnames(cory)) {
  G <- rownames(cory)[abs(cory[, CO]) > 0.5]
  write.csv(G, file = file.path(dp, paste(CO, "-corr.csv", sep='')))
}
```

```{r tfcor}
tfdata <- t(megadata[isTF,])
tfcor <- cor(tfdata)
oc <- order(bcs$Cluster)
tfcor <- tfcor[oc, oc]
save(tfcor, file="../scratch/tfcor.rda")
image(tfcor, col=redgreen(64), zlim=c(-1,1))
heatmap(tfcor, scale="none", col=redgreen(64), zlim=c(-1,1))
```

# Finding the best representative of each cluster

```{r clusterReps}
gclust <- read.csv(file.path("..", "results", "geneClust.csv"))
temp <- cory[as.character(gclust$Hs.SYMBOL),]
w <- apply(temp, 2, which.max)
load("../results/thisRes.Rda")
clusterReps <- thisResult$clusters[w,]
save(clusterReps, file="../results/clusterReps.Rda")
clusterReps
```

# Fancy

```{r thirty, fig.width=20,fig.height=20, fig.cap="Name that cancer in 30 TFs."}
chuckles <- megadata[as.character(clusterReps$RNA),]
library(Rtsne)
foo <- Rtsne(t(chuckles), initial_dims = 30)
colvec <- colorScheme[as.character(allAnno$cancerType)]
sym <- c(16, 2, 8)[as.numeric(allAnno$Cat)]
xcent <- tapply(foo$Y[,1], list(allAnno$cancerType), median)
ycent <- tapply(foo$Y[,2], list(allAnno$cancerType), median)

plot(foo$Y, pch=sym, col=colvec, xlab="T1", ylab="T2", cex=0.8)
text(xcent, ycent, levels(allAnno$cancerType), col='black', cex=1.5)
```

# Appendix
This analysis was performed in the following directory.
```{r where}
getwd()
```
This analysis was performed using the following R packages.
```{r si}
sessionInfo()
```
